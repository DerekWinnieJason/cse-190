package com.floc.floc.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.floc.floc.ListViewAdapters.CustomFriendsAdapter;
import com.floc.floc.AdapterItems.Friend;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindFriendsFragment extends Fragment implements AbsListView.OnItemClickListener,
        AbsListView.OnItemLongClickListener{

    EditText SearchBar;
    ArrayList<Friend> friends;
    ListView listView;

    private CustomFriendsAdapter mAdapter;
    ProgressDialog dialog;

    public static FindFriendsFragment newInstance() {
        FindFriendsFragment fragment = new FindFriendsFragment();
        return fragment;
    }

    public FindFriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        friends = new ArrayList<Friend>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_find_friends, container, false);
        listView = (ListView)view.findViewById(R.id.find_friends_listView);
        SearchBar = (EditText)view.findViewById(R.id.find_friends_searchBar);
        getActivity().setTitle("Find Friends");
        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        SearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    private void DisplayFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StrangerProfileFragment fragment = StrangerProfileFragment.newInstance(
                friends.get(position).getObjectID());
        DisplayFragment(fragment);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mAdapter != null) {
            System.out.println("HERE: 0");
            mAdapter.clear();
        }
        dialog = ProgressDialog.show(this.getActivity(),"", "Loading friends...");
        friends = new ArrayList<Friend>();
        ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                JSONArray array = ParseUser.getCurrentUser().getJSONArray("friends");
                // retrieve friends, input into arraylist, then check if same as here
                System.out.println("HERE: 1");
                final ArrayList<String> myFriends = new ArrayList<String>();

                if(array != null) {
                    try {
                        for (int i = 0; i < array.length(); i++) {
                            myFriends.add(array.getString(i));
                            System.out.println("HERE: 2: " + array.getString(i));

                        }
                    } catch (JSONException exc) {
                        Log.d(ParseApplication.TAG, exc.toString());
                        dialog.dismiss();
                    }
                }
                else{
                    dialog.dismiss();
                }
                System.out.println("HERE: 3");

                myFriends.add(ParseUser.getCurrentUser().getObjectId());

                final ArrayList<Friend> toReplace = new ArrayList<Friend>();
                ParseQuery query = new ParseQuery("_User");

                query.findInBackground(new FindCallback() {
                    @Override
                    public void done(List list, ParseException e) {
                        for(int i=0; i<list.size(); i++) {
                            ParseUser user = (ParseUser) list.get(i);
                            if(!myFriends.contains(user.getObjectId())) {
                                Friend friend = new Friend();
                                friend.setFacebookID(user.getString("facebookID"));
                                friend.setName(user.getString("name"));
                                friend.setObjectID(user.getObjectId());
                                toReplace.add(friend);
                            }
                        }
                        friends = toReplace;

                        mAdapter = new CustomFriendsAdapter(getActivity(),
                                R.layout.listview_friend_item, friends);

                        listView.setAdapter(mAdapter);
                        dialog.dismiss();
                    }
                });
            }
        });
    }
/*
    private void PopulateListView(final List list, final int num) {
        if(list == null) {
            dialog.dismiss();
            return;
        }
        if(num == list.size()) {
            mAdapter = new CustomFriendsAdapter(getActivity(), R.layout.listview_friend_item, friends);
            listView.setAdapter(mAdapter);
            dialog.dismiss();
            return;
        }
        ParseUser user = (ParseUser) list.get(num);
        user.fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                Fri
            }
        });
    }*/
}
