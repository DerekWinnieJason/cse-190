package com.floc.floc.Fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.floc.floc.Activities.MainActivity;
import com.floc.floc.FlocEvent;
import com.floc.floc.ListViewAdapters.HorizontalViewAdapter;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.meetme.android.horizontallistview.HorizontalListView;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventInfoFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    String eventID;
    FlocEvent event;

    ParseImageView picture;

    TextView name;
    TextView creator;
    TextView about;
    TextView going_num;
    TextView declined_num;
    TextView invited_num;

    HorizontalListView listView;

    Button directions;
    Button status;

    ProgressDialog dialog;

    public EventInfoFragment() {
        // Required empty public constructor
    }

    public static EventInfoFragment newInstance(String eventID) {
        EventInfoFragment fragment = new EventInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, eventID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (getArguments() != null) {
            this.eventID = getArguments().getString(ARG_PARAM1);

        }

        final View view = inflater.inflate(R.layout.fragment_event_info, container, false);
        getActivity().setTitle("Event Details");
        dialog = ProgressDialog.show(this.getActivity(), "", "Loading profile...");
        picture = (ParseImageView)view.findViewById(R.id.event_info_image);
        name = (TextView)view.findViewById(R.id.event_info_name);
        creator = (TextView)view.findViewById(R.id.event_info_host);
        directions = (Button)view.findViewById(R.id.event_info_directions);
        status = (Button)view.findViewById(R.id.event_info_status);
        listView = (HorizontalListView)view.findViewById(R.id.event_info_horizontalList);
        about = (TextView)view.findViewById(R.id.event_info_about);
        going_num = (TextView)view.findViewById(R.id.event_info_going_num);
        declined_num = (TextView)view.findViewById(R.id.event_info_declined_num);
        invited_num = (TextView)view.findViewById(R.id.event_info_invited_num);

        directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickDirections();
            }
        });

        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickStatus();
            }
        });

        ParseQuery query = new ParseQuery("Events");
        query.getInBackground(this.eventID, new GetCallback() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e == null) {
                    event = (FlocEvent) parseObject;
                    setUpLayout();
                }
                else {
                    Log.d(ParseApplication.TAG, e.toString());
                }
            }
        });

        return view;
    }

    private void OnClickStatus() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Event Status")
                .setPositiveButton("Decline", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateEvent("decline");
                    }
                })
                .setNegativeButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateEvent("accept");
                    }
                }).show();
    }

    private void updateEvent(final String action) {
        final String id = ParseUser.getCurrentUser().getObjectId();
        final ArrayList<String> toRemove = new ArrayList<>();
        toRemove.add(id);
        final String first, second;
        if(action.equals("accept")) {
            first = "Declined";
            second = "Accepted";
        }
        else {
            first = "Accepted";
            second = "Declined";
        }

        event.removeAll(first, toRemove);
        event.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                event.removeAll("Invited", toRemove);
                event.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        event.addUnique(second, id);
                        event.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                updateUser(action);
                            }
                        });
                    }
                });
            }
        });
    }

    private void updateUser(String action) {
        final ParseUser user = ParseUser.getCurrentUser();
        final ArrayList<String> toRemove = new ArrayList<>();
        toRemove.add(eventID);
        final String first, second;
        if(action.equals("accept")) {
            first = "declined_events";
            second = "accepted_events";
        }
        else {
            first = "accepted_events";
            second = "declined_events";
        }

        user.removeAll(first, toRemove);
        user.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                user.removeAll("invited_events", toRemove);
                user.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        user.addUnique(second, eventID);
                        user.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                setUpLayout();
                            }
                        });
                    }
                });
            }
        });
    }
    private void OnClickDirections() {

        if(MainActivity.lastKnownLocation == null) {
            Toast.makeText(getActivity(), "Activate your GPS for directions.",
                    Toast.LENGTH_LONG).show();
            return;
        }

        Location loc = event.getLocation();
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" +
                        MainActivity.lastKnownLocation.getLatitude() + ","
                        + MainActivity.lastKnownLocation.getLongitude() +
                        "&daddr=" + loc.getLatitude() + "," + loc.getLongitude()));
        startActivity(intent);
    }

    private boolean checkEventInvited() {
        try {
            JSONArray accepted = ParseUser.getCurrentUser().getJSONArray("accepted_events");

            if(accepted != null) {
                for (int i = 0; i < accepted.length(); i++) {
                    if (accepted.getString(i).equals(eventID)) {
                        return true;
                    }
                }
            }

            accepted = ParseUser.getCurrentUser().getJSONArray("invited_events");

            if(accepted != null) {
                for (int i = 0; i < accepted.length(); i++) {
                    if (accepted.getString(i).equals(eventID)) {
                        return true;
                    }
                }
            }

            accepted = ParseUser.getCurrentUser().getJSONArray("declined_events");

            if(accepted != null) {
                for (int i = 0; i < accepted.length(); i++) {
                    if (accepted.getString(i).equals(eventID)) {
                        return true;
                    }
                }
            }

        } catch(JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
        return false;
    }

    private void setUpLayout() {
        ParseFile photo = event.getParseFile("Picture");
        JSONArray accepted = event.getJSONArray("Accepted");
        JSONArray declined = event.getJSONArray("Declined");
        JSONArray invited = event.getJSONArray("Invited");
        ArrayList<String> accepts = new ArrayList<>();

        if(photo != null) {
            picture.setParseFile(photo);
            picture.loadInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    dialog.dismiss();
                }
            });
        }
        else {
            picture.setImageDrawable(getResources().getDrawable(R.drawable.default_event));
            dialog.dismiss();
        }
        picture.setColorFilter(Color.rgb(80, 80, 80), PorterDuff.Mode.DARKEN);

        name.setText(event.getName());
        creator.setText("Host: " + event.getCreatorName());

        about.setMovementMethod(new ScrollingMovementMethod());
        about.setText("About: " + "\n" + event.getDetails());

        if(!checkEventInvited()) {
            status.setEnabled(false);
            directions.setEnabled(false);
        }

        try {
            if(accepted != null) {
                for(int i=0; i<accepted.length(); i++) {
                    accepts.add(accepted.getString(i));
                }

                HorizontalViewAdapter adapter = new HorizontalViewAdapter(getActivity(),
                        R.layout.profilepicture_event_info, "info", accepts);

                listView.setAdapter(adapter);
            }

        } catch (JSONException exc) {
            Log.d(ParseApplication.TAG, exc.toString());
        }

        if(accepted == null) going_num.setText("0");
        else going_num.setText(String.valueOf(accepted.length()));

        if(declined == null) declined_num.setText("0");
        else declined_num.setText(String.valueOf(declined.length()));

        if(invited == null) invited_num.setText("0");
        else invited_num.setText(String.valueOf(invited.length()));

    }

}
