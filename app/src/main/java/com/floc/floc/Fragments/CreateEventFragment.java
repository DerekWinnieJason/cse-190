package com.floc.floc.Fragments;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Scroller;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.floc.floc.Floc;
import com.floc.floc.FlocEvent;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateEventFragment extends Fragment implements
        CreateEventMapFragment.OnMapInteractionListener,
        CreateEventFriendFragment.OnFriendInteractionListener {

    Location loc;
    Double radius = 2.0;
    HashSet<String> invited_friends;

    private EditText name;
    private EditText details;
    private Button my_location;
    private Button choose_location;
    private Spinner choose_group;
    private Button invite;
    private Button photo;
    private SeekBar radiusSeekBar;
    private TextView radiusTextView;
    private Button submit;
    private Button cancel;

    boolean location_is_set;
    ParseFile picture;

    Floc floc;

    String eventID = "";

    public CreateEventFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        invited_friends = new HashSet<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_event, container, false);

        this.getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.
                SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getActivity().setTitle("Create Event");
        name = (EditText)view.findViewById(R.id.create_event_name);
        details = (EditText)view.findViewById(R.id.create_event_details);
        my_location = (Button)view.findViewById(R.id.create_event_my_location);
        choose_location = (Button)view.findViewById(R.id.create_event_choose_location);
        choose_group = (Spinner)view.findViewById(R.id.create_event_choose_group);
        invite = (Button)view.findViewById(R.id.create_event_choose_friends);
        photo = (Button)view.findViewById(R.id.create_event_choose_photo);
        radiusSeekBar = (SeekBar)view.findViewById(R.id.create_event_radius);
        radiusTextView = (TextView)view.findViewById(R.id.create_event_radius_num);
        submit = (Button)view.findViewById(R.id.create_event_submit);
        cancel = (Button)view.findViewById(R.id.create_event_cancel);

        name.setMaxLines(1);

        details.setScroller(new Scroller(this.getActivity().getApplicationContext()));
        details.setMaxLines(2);
        details.setVerticalScrollBarEnabled(true);
        details.setMovementMethod(new ScrollingMovementMethod());

        my_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickMyLocation();
            }
        });

        choose_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickChooseLocation();
            }
        });

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickInviteFriends();
            }
        });

        photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickChoosePhoto();
            }
        });
        radiusSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    setRadiusTextView();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Necessary to implement OnSeekBarChangeListener
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Necessary to implement OnSeekBarChangeListener
            }
        });

        ArrayList<String> x = new ArrayList<>();
        x.add("Select a floc to invite");

        ArrayAdapter a = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, x);
        choose_group.setAdapter(a);

        final ParseQueryAdapter<Floc> adapter = new ParseQueryAdapter(getActivity(),
                new ParseQueryAdapter.QueryFactory<Floc>() {
                    public ParseQuery<Floc> create() {
                        List<ParseQuery<Floc>> queries = new ArrayList<ParseQuery<Floc>>();
                        ParseQuery<Floc> query = new ParseQuery<Floc>("Groups");
                        ArrayList<String> a = new ArrayList<>();
                        a.add(ParseUser.getCurrentUser().getObjectId());
                        query.whereContainedIn("Members", a);
                        ParseQuery<Floc> defaultText = new ParseQuery<Floc>("Groups");
                        defaultText.whereEqualTo("Name", "Select a floc to invite");
                        queries.add(defaultText);
                        queries.add(query);
                        ParseQuery<Floc> f = ParseQuery.or(queries);
                        f.orderByDescending("default");
                        return f;
                    }
                }, android.R.layout.simple_list_item_1);
        adapter.setTextKey("Name");

        choose_group.setAdapter(adapter);

        choose_group.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Floc f = adapter.getItem(position);
                if(f.getName().equals("Select a floc to invite")) {
                    floc = null;
                }
                else {
                    floc = f;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                floc = null;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickSubmit();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickCancel();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    private void OnClickMyLocation() {
        location_is_set = true;
        my_location.setEnabled(false);
    }

    private void OnClickChooseLocation() {
        CreateEventMapFragment fragment;
        if(loc == null) {
            fragment = CreateEventMapFragment.newInstance(-500.0, -500.0);
        }
        else {
            fragment = CreateEventMapFragment.newInstance(loc.getLatitude(), loc.getLongitude());
        }

        my_location.setEnabled(true);

        fragment.setMapListener(this);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    private void OnClickInviteFriends() {
        CreateEventFriendFragment fragment = CreateEventFriendFragment.newInstance();
        fragment.SetHashSet(invited_friends);
        fragment.setFriendListener(this);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void OnClickChoosePhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    private void OnClickSubmit() {
        if(name.getText().toString().matches("")) {
            createToast("Please enter your event name.");
            return;
        }
        if(!location_is_set) {
            createToast("Please enter your event location.");
            return;
        }
        if((invited_friends == null || invited_friends.size() == 0) && floc == null) {
            createToast("Choose friends to invite.");
            return;
        }

        Log.d(ParseApplication.TAG, "TESTINGGG");
        JSONArray toAdd = floc.getJSONArray("Members");
        try {
            for(int i=0; i<toAdd.length(); i++) {
                if(!toAdd.getString(i).equals(ParseUser.getCurrentUser().getObjectId())) {
                    invited_friends.add(toAdd.getString(i));
                    Log.d(ParseApplication.TAG, "MEMBERS OF " + floc.getName() + ": " + toAdd.getString(i));
                }
            }
        } catch (JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
        Log.d(ParseApplication.TAG, "TESTINGGG2");

        final FlocEvent flocEvent = new FlocEvent();
        flocEvent.setName(name.getText().toString());
        flocEvent.setDetails(details.getText().toString());

        if(loc != null) {
            flocEvent.setLocationIsCreator(false);
            flocEvent.setLocation(loc.getLatitude(), loc.getLongitude());
        }
        else {
            flocEvent.setLocationIsCreator(true);
        }

        flocEvent.setCreator(ParseUser.getCurrentUser());
        flocEvent.setCreatorName(ParseApplication.userName);
        flocEvent.setCreatorFacebookID(ParseUser.getCurrentUser());
        flocEvent.setRadius(radius);
        flocEvent.addUnique("Accepted", ParseUser.getCurrentUser().getObjectId());

        //List<String> list = Arrays.asList(invited);
        invited_friends.remove(null);

        flocEvent.addAllUnique("Invited", invited_friends);

        Object[] x = invited_friends.toArray();

        final ArrayList<String> f = new ArrayList<String>();

        for(int i=0;i<x.length;i++) {
            f.add(x[i].toString());
        }
        flocEvent.saveInBackground( new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e == null) {
                    if(picture != null) {
                        flocEvent.put("Picture", picture);
                        flocEvent.saveInBackground();
                    }

                    ParseUser.getCurrentUser().addUnique("created_events", flocEvent.getObjectId());
                    ParseUser.getCurrentUser().addUnique("accepted_events", flocEvent.getObjectId());
                    ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            final HashMap<String, Object> params = new HashMap<String, Object>();
                            params.put("name", ParseApplication.userName);
                            params.put("eventID", flocEvent.getObjectId());
                            params.put("ids", f);
                            ParseCloud.callFunctionInBackground("SendEventInvitations", params);
                                    /*,
                                    new FunctionCallback<Object>() {
                                        @Override
                                        public void done(Object o, ParseException e) {
                                            ParseCloud.callFunctionInBackground("AddEventToInvitedList", params);
                                        }
                                    });*/
                            eventID = flocEvent.getObjectId();
                            DisplayEvent();
                        }
                    });
                }
                else {
                    createToast("Error Saving: " + e.getMessage());
                }
            }
        });
    }

    private void DisplayEvent() {
        EventInfoFragment fragment = EventInfoFragment.newInstance(eventID);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    private void OnClickCancel() {
        //hideKeyboard();
        this.getActivity().getSupportFragmentManager().popBackStack();
    }
    private void createToast(String msg) {
        Toast.makeText(this.getActivity().getApplicationContext(), msg,
                Toast.LENGTH_SHORT).show();
    }

    private void setRadiusTextView() {
        NumberFormat f = new DecimalFormat("#0.00");
        final double max = 25;

        if(radiusSeekBar.getProgress() != 0) {
            radius = (double) radiusSeekBar.getProgress() / max;
            radiusTextView.setText(f.format(radius) + " miles");
        }
    }


    private void hideKeyboard() {
        InputMethodManager im = (InputMethodManager) getActivity().
                getSystemService(Context.INPUT_METHOD_SERVICE);

        if(im != null) {
            im.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null || resultCode != getActivity().RESULT_OK) {
            return;
        }
        Uri selectedImageUri = data.getData();
        String selectedImagePath = getPath(selectedImageUri);

        try {
            byte[] pic = getByteArrayFromImage(selectedImagePath);
            picture = new ParseFile("event_picture.jpg", pic);
        }
        catch(IOException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
    }

    private String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private byte[] getByteArrayFromImage(String filePath) throws FileNotFoundException, IOException {
        File file = new File(filePath);
        FileInputStream input = new FileInputStream(file);

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];

        try {
            for(int readNum; (readNum = input.read(buf)) != -1;) {
                output.write(buf, 0, readNum);
            }
        } catch (IOException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }

        byte[] bytes = output.toByteArray();
        return bytes;
    }

    @Override
    public void onMapInteraction(Location location) {
        this.loc = location;
        location_is_set = true;
    }

    @Override
    public void onFriendInteraction(HashSet<String> invited) {


        Object[] toRet = invited.toArray();
        String[] friends = new String[toRet.length];

        for(int i=0; i<toRet.length; i++) {
            friends[i] = toRet[i].toString();
            Log.d(ParseApplication.TAG, "TESTS I : " + i + " " + friends[i]);
        }

        this.invited_friends = invited;
    }
}
