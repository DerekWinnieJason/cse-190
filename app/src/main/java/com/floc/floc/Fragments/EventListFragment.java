package com.floc.floc.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.floc.floc.FlocEvent;
import com.floc.floc.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventListFragment extends Fragment implements AbsListView.OnItemClickListener,
        AbsListView.OnItemLongClickListener{

    private final int accepted_click = 0;
    private final int declined_click = 1;
    private final int expired_click = 2;
    private int clicked;

    ListAdapter accepted;
    ListAdapter declined;
    ListAdapter expired;

    ListView listView;
    String UserObjectID;
    boolean read_only;

    private static final String ARG_PARAM1 = "param1";

    public EventListFragment() {
        // Required empty public constructor
    }

    public static EventListFragment newInstance(String userID) {
        EventListFragment fragment = new EventListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, userID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_event_list, container, false);

        if (getArguments() != null) {
            this.UserObjectID = getArguments().getString(ARG_PARAM1);

            if(this.UserObjectID.equals(ParseUser.getCurrentUser().getObjectId())) {
                read_only = false;
            }
            else {
                read_only = true;
            }
        }

        ParseQuery query = ParseQuery.getQuery("_User");

        query.getInBackground(UserObjectID, new GetCallback() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                SetUpListView((ParseUser)parseObject, view);
            }
        });

        SetUpButtons(view);

        return view;
    }

    private void SetUpListView(ParseUser user, View view) {/*
        ArrayList<String> accept = new ArrayList<String>();
        ArrayList<String> decline = new ArrayList<String>();
        ArrayList<String> expire = new ArrayList<String>();

        try {
            JSONArray acceptJSON = user.getJSONArray("accepted_events");
            JSONArray declineJSON = user.getJSONArray("declined_events");
            JSONArray expireJSON = user.getJSONArray("expired_events");
            if(acceptJSON != null) {
                for (int i = 0; i < acceptJSON.length(); i++) {
                    accept.add(acceptJSON.getString(i));
                }
            }

            if(declineJSON != null) {
                for (int i = 0; i < declineJSON.length(); i++) {
                    decline.add(declineJSON.getString(i));
                }
            }

            if(expireJSON != null) {
                for (int i = 0; i < expireJSON.length(); i++) {
                    expire.add(expireJSON.getString(i));
                }
            }

        } catch (JSONException exc) {
            Log.d(ParseApplication.TAG, exc.toString());
        }
*/


        //ADD TO ADAPTER AND ADD TO LISTVIEW
        listView = (ListView) view.findViewById(R.id.events);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        listView.setAdapter(accepted);
        clicked = accepted_click;
    }

    private void SetUpButtons(View view) {
        Button button_accepted = (Button) view.findViewById(R.id.accepted);
        Button button_declined = (Button) view.findViewById(R.id.declined);
        Button button_expired = (Button) view.findViewById(R.id.expired);

        button_accepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setAdapter(accepted);
                clicked = accepted_click;
            }
        });

        button_declined.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setAdapter(declined);
                clicked = declined_click;
            }
        });

        button_expired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.setAdapter(expired);
                clicked = expired_click;
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        FlocEvent event = null;



        EventInfoFragment fragment = new EventInfoFragment();
        DisplayFragment(fragment);
    }

    private void DisplayFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }
}
