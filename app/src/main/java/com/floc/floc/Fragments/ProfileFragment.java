package com.floc.floc.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.floc.floc.AdapterItems.EventItemInfo;
import com.floc.floc.CustomProfilePictureView;
import com.floc.floc.FlocEvent;
import com.floc.floc.ListViewAdapters.CustomEventsAdapter;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements View.OnLongClickListener{

    private static final String ARG_PARAM1 = "param1";

    ParseObject user;

    String UserObjectID;
    ParseImageView cover;
    ProgressDialog dialog;
    ProgressDialog eventsDialog;

    CustomProfilePictureView userProfilePictureView;
    TextView name, friends, flocs;
    Button created, accepted, declined;
    ListView listView;
    ArrayList<EventItemInfo> eventInfos;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String userID) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, userID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (getArguments() != null) {
            this.UserObjectID = getArguments().getString(ARG_PARAM1);
        }

        dialog = ProgressDialog.show(this.getActivity(), "", "Loading profile...");

        getActivity().setTitle("Profile");
        final View view = inflater.inflate(R.layout.fragment_profile, container, false);

        userProfilePictureView = (CustomProfilePictureView)view.findViewById(R.id.userProfilePicture);
        name = (TextView)view.findViewById(R.id.profile_name);
        friends = (TextView)view.findViewById(R.id.num_friends);
        flocs = (TextView)view.findViewById(R.id.num_flocs);
        cover = (ParseImageView)view.findViewById(R.id.profile_cover);

        created = (Button)view.findViewById(R.id.profile_created);
        accepted = (Button)view.findViewById(R.id.profile_accepted);
        declined = (Button) view.findViewById(R.id.profile_declined);
        listView = (ListView)view.findViewById(R.id.profile_listView);

        if(!UserObjectID.equals(ParseUser.getCurrentUser().getObjectId())) {
            accepted.setEnabled(false);
            accepted.setBackgroundColor(R.id.grayscale);
            declined.setEnabled(false);
            declined.setBackgroundColor(R.id.grayscale);
            created.setBackgroundColor(getResources().getColor(R.color.theme));
        }
        else {
            declined.setBackgroundColor(R.id.grayscale);
            created.setBackgroundColor(R.id.grayscale);
        }

        ParseQuery query = ParseQuery.getQuery("_User");
        query.getInBackground(UserObjectID, new GetCallback() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                user = parseObject;
                setUpLayout();
            }
        });

        created.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickCreated();
            }
        });

        accepted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickAccepted();
            }
        });

        declined.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickDeclined();
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    private void setUpLayout() {
        name.setText(user.getString("name"));
        userProfilePictureView.setProfileId(user.getString("facebookID"));
        JSONArray f = user.getJSONArray("friends");
        JSONArray fs = user.getJSONArray("group");
        JSONArray input;

        if(UserObjectID.equals(ParseUser.getCurrentUser().getObjectId())) {
            input = user.getJSONArray("accepted_events");
        }
        else {
            input = user.getJSONArray("created_events");
        }

        setUpListViews(input, 0);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EventInfoFragment fragment = EventInfoFragment.newInstance(eventInfos.get(position).getEventID());
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        });

        if(f == null || f.length() == 0) {
            friends.setText("0 friends");
        }
        else {
            friends.setText(f.length() + " friends");
        }

        if(fs == null || fs.length() == 0) {
            flocs.setText("0 flocs");
        }
        else {
            flocs.setText(fs.length() + " flocs");
        }

        cover.setOnLongClickListener(this);

        friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ParseFile photo = user.getParseFile("cover_photo");

        if(photo != null) {
            cover.setParseFile(photo);
            cover.loadInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    cover.setVisibility(View.VISIBLE);
                }
            });
        }
        else {
            cover.setImageDrawable(getResources().getDrawable(R.drawable.default_cover));
        }
        cover.setColorFilter(Color.rgb(80,80,80), PorterDuff.Mode.DARKEN);
        dialog.dismiss();

    }

    private void setUpListViews(final JSONArray array, final int num) {

        if(num == 0 && array != null) {
            eventInfos = new ArrayList<>();
            eventsDialog = ProgressDialog.show(this.getActivity(), "", "Loading events...");
        }

        if(array == null || num == array.length()) {
            if(eventsDialog != null) {
                eventsDialog.dismiss();
            }
            if(eventInfos == null) {
                eventInfos = new ArrayList<>();
            }

            CustomEventsAdapter mAdapter = new CustomEventsAdapter(getActivity(),
                    R.layout.listview_event_item, eventInfos);
            listView.setAdapter(mAdapter);
            return;
        }

        try {
            ParseQuery q = ParseQuery.getQuery("Events");
            q.getInBackground(array.getString(num), new GetCallback() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if(e == null) {
                        FlocEvent event = (FlocEvent) parseObject;
                        EventItemInfo info = new EventItemInfo();

                        info.setEventCreator(event.getCreatorName());
                        info.setEventName(event.getName());
                        info.setEventID(event.getObjectId());

                        eventInfos.add(info);

                        setUpListViews(array, num + 1);
                    }
                    else {
                        Log.d(ParseApplication.TAG, e.toString());
                        setUpListViews(array, num + 1);
                        eventsDialog.dismiss();
                    }
                }
            });

        } catch(JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
            eventsDialog.dismiss();
        }
    }

    private void OnClickCoverPhoto() {
        if(!ParseUser.getCurrentUser().getObjectId().equals(UserObjectID)) {
            return;
        }

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    private void OnClickCreated() {
        created.setBackgroundColor(getResources().getColor(R.color.theme));
        accepted.setBackgroundColor(R.id.grayscale);
        declined.setBackgroundColor(R.id.grayscale);
        JSONArray array = user.getJSONArray("created_events");
        setUpListViews(array, 0);
    }

    private void OnClickAccepted() {
        created.setBackgroundColor(R.id.grayscale);
        accepted.setBackgroundColor(getResources().getColor(R.color.theme));
        declined.setBackgroundColor(R.id.grayscale);
        JSONArray array = user.getJSONArray("accepted_events");
        setUpListViews(array, 0);
    }

    private void OnClickDeclined() {
        created.setBackgroundColor(R.id.grayscale);
        accepted.setBackgroundColor(R.id.grayscale);
        declined.setBackgroundColor(getResources().getColor(R.color.theme));
        JSONArray array = user.getJSONArray("declined_events");
        setUpListViews(array, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null || resultCode != getActivity().RESULT_OK) {
            return;
        }
        Uri selectedImageUri = data.getData();
        String selectedImagePath = getPath(selectedImageUri);
        try {
            cover.setImageURI(selectedImageUri);
        }
        catch(OutOfMemoryError e) {
            Toast.makeText(getActivity(), "Image size too large",
                    Toast.LENGTH_LONG).show();
            return;
        }
        try {
            byte[] pic = getByteArrayFromImage(selectedImagePath);
            final ParseFile toUpload = new ParseFile("cover.jpg", pic);
            toUpload.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    user.put("cover_photo", toUpload);
                    user.saveInBackground();
                }
            });

        }
        catch(IOException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
    }

    private String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private byte[] getByteArrayFromImage(String filePath) throws FileNotFoundException, IOException {
        File file = new File(filePath);
        FileInputStream input = new FileInputStream(file);

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];

        try {
            for(int readNum; (readNum = input.read(buf)) != -1;) {
                output.write(buf, 0, readNum);
            }
        } catch (IOException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }

        byte[] bytes = output.toByteArray();
        return bytes;
    }


    @Override
    public boolean onLongClick(View v) {
        OnClickCoverPhoto();
        return true;
    }

}
