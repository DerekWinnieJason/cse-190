package com.floc.floc.Fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.floc.floc.CustomProfilePictureView;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class StrangerProfileFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private final static int REQUEST_SENT = 1;
    private final static int REQUEST_RECEIVED = 2;

    ParseObject user;

    String UserObjectID;
    ParseImageView cover;
    ProgressDialog dialog;
    CustomProfilePictureView userProfilePictureView;
    TextView name;

    boolean dialogNotShown = true;
    int type = 0;
    Button button;

    public static StrangerProfileFragment newInstance(String userID) {
        StrangerProfileFragment fragment = new StrangerProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, userID);
        fragment.setArguments(args);
        return fragment;
    }

    public StrangerProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            UserObjectID = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_stranger_profile, container, false);
        getActivity().setTitle("Profile");
        dialog = ProgressDialog.show(this.getActivity(),"", "Loading profile...");

        cover = (ParseImageView)view.findViewById(R.id.profile_cover);
        userProfilePictureView = (CustomProfilePictureView) view.findViewById(R.id.userProfilePicture);
        name = (TextView) view.findViewById(R.id.profile_name);
        button = (Button)view.findViewById(R.id.friend_request);

        ParseUser.getCurrentUser().fetchInBackground( new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {

                ParseQuery query = new ParseQuery("_User");
                query.getInBackground(UserObjectID, new GetCallback() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        user = parseObject;
                        SetUpButton();
                        SetUpLayout();
                    }
                });

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickButton();
            }
        });

        return view;
    }

    private void SetUpLayout() {
        name.setText(user.getString("name"));
        userProfilePictureView.setProfileId(user.getString("facebookID"));

        ParseFile photo = user.getParseFile("cover_photo");
        if(photo != null) {
            cover.setParseFile(photo);
            cover.loadInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    cover.setVisibility(View.VISIBLE);
                    dialog.dismiss();
                }
            });
        }
        else {
            cover.setImageDrawable(getResources().getDrawable(R.drawable.default_cover));
            dialog.dismiss();
        }
        cover.setColorFilter(Color.rgb(80, 80, 80), PorterDuff.Mode.DARKEN);
    }

    private void SetUpButton() {
        JSONArray received = ParseUser.getCurrentUser().getJSONArray("sent_friend_request");
        JSONArray sent = ParseUser.getCurrentUser().getJSONArray("received_friend_request");

        Boolean found = false;
        try {
            if(received != null) {
                for (int i = 0; i < received.length(); i++) {
                    if (UserObjectID.equals(received.getString(i))) {
                        type = REQUEST_SENT;
                        button.setEnabled(false);
                        found = true;
                        break;
                    }
                }
            }

            if(sent != null && !found) {
                for(int i=0; i<sent.length(); i++) {
                    if(UserObjectID.equals(sent.getString(i))) {
                        type = REQUEST_RECEIVED;
                        break;
                    }
                }
            }
        }
        catch(JSONException exc) {
            Log.d(ParseApplication.TAG, exc.toString());
        }

        if(type == REQUEST_SENT) {
            button.setText("Friend request sent.");
            button.setEnabled(false);
        }
        else if(type == REQUEST_RECEIVED) {
            button.setText("Respond to friend request.");
        }
    }

    private void OnClickButton() {

        if(type == REQUEST_RECEIVED) {
            if(dialogNotShown) {
                ShowAcceptDialog();
                dialogNotShown = false;
            }
            return;
        }

        JSONObject toSend = new JSONObject();

        try {
            toSend.put("type", "friend_request");
            toSend.put("objectId", ParseUser.getCurrentUser().getObjectId());
            toSend.put("alert", ParseApplication.userName + " has sent you a friend request!");
            toSend.put("name", ParseApplication.userName);
        }
        catch (JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("request_to", UserObjectID);
        params.put("request_from", ParseUser.getCurrentUser().getObjectId());

        ParseCloud.callFunctionInBackground("SendFriendRequest", params);

        ParseQuery innerQuery = new ParseQuery("_User");
        innerQuery.whereEqualTo("objectId", UserObjectID);

        ParseQuery pushQuery = ParseQuery.getQuery("_Installation");
        pushQuery.whereMatchesQuery("user", innerQuery);

        ParsePush push = new ParsePush();
        push.setQuery(pushQuery);
        push.setData(toSend);
        push.sendInBackground();

        ParseUser.getCurrentUser().add("sent_friend_request", UserObjectID);
        ParseUser.getCurrentUser().saveInBackground();

        button.setText("Friend request sent.");
        button.setEnabled(false);
    }

    private void ShowAcceptDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Friend request from " + name + ".")
                .setPositiveButton("Decline", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ArrayList<String> toRemove = new ArrayList<String>();
                        toRemove.add(UserObjectID);
                        ParseUser.getCurrentUser().removeAll("received_friend_request", toRemove);
                        ParseUser.getCurrentUser().saveInBackground();

                        HashMap<String, Object> params = new HashMap<String, Object>();
                        params.put("toSendRejection", UserObjectID);
                        params.put("rejected_from", ParseUser.getCurrentUser().getObjectId());

                        ParseCloud.callFunctionInBackground("RejectedFriendRequest", params);
                        dialogNotShown = true;
                        button.setText("Send Friend Request.");
                        type = 0;
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialogNotShown = true;
                    }
                })
                .setNeutralButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HashMap<String, Object> params = new HashMap<String, Object>();
                        params.put("name", ParseApplication.userName);
                        params.put("request_from", UserObjectID);
                        params.put("request_to", ParseUser.getCurrentUser().getObjectId());

                        ParseCloud.callFunctionInBackground("AddFriends", params,
                                new FunctionCallback<Object>() {
                            @Override
                            public void done(Object o, ParseException e) {
                                ParseUser.getCurrentUser().fetchInBackground();
                            }
                        });
                        ProfileFragment fragment = ProfileFragment.newInstance(UserObjectID);

                        DisplayFragment(fragment);
                        /********* POSSIBLE TO SOLVE FRIEND LIST NOT UPDATING ON SAVE
                        SharedPreferences pref = getActivity().
                                getSharedPreferences("preferences", getActivity().MODE_PRIVATE);

                        Set<String> toChange = pref.getStringSet("friendsAdded", null);

                        if(toChange == null) {
                            toChange = new HashSet<String>();
                        }

                        SharedPreferences.Editor toSave = getActivity().getSharedPreferences(
                                "preferences", getActivity().MODE_PRIVATE).edit();

                        toChange.add(userID);

                        toSave.putStringSet("friendsAdded",toChange);
                        toSave.apply();*/
                    }
                }).setCancelable(false).show();
    }

    private void DisplayFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
