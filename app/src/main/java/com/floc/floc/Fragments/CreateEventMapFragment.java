package com.floc.floc.Fragments;


import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.floc.floc.Activities.MainActivity;
import com.floc.floc.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateEventMapFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapLongClickListener, AdapterView.OnItemClickListener {

    private static final String LOG_TAG = "ExampleApp";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_DETAILS = "/details";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    private static final String API_KEY = "AIzaSyB9F6TvkDkqXdE6gYP1tqCX71tQ6TyAtFQ";

    private OnMapInteractionListener mListener;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    GoogleMap map;
    SupportMapFragment MapFragment;
    Location location;
    Button button;
    Marker marker;
    View x;
    ArrayList<String> places;
    ArrayList<String> resultList;

    AutoCompleteTextView autoCompView;

    public static CreateEventMapFragment newInstance(Double lat, Double lng) {
        CreateEventMapFragment fragment = new CreateEventMapFragment();
        Bundle args = new Bundle();
        args.putDouble(ARG_PARAM1, lat);
        args.putDouble(ARG_PARAM2, lng);
        fragment.setArguments(args);
        return fragment;
    }

    public CreateEventMapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_event_map, container, false);

       // x = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.marker, null);
        //CustomProfilePictureView p = (CustomProfilePictureView)x.findViewById(R.id.map_picture);
        //p.setProfileId(ParseApplication.userFacebookID);

        if (getArguments() != null) {
            Double param1 = getArguments().getDouble(ARG_PARAM1);
            Double param2 = getArguments().getDouble(ARG_PARAM2);

            if(param1 != -500.0 && param2 != -500.0) {
                Double latitude = param1;
                Double longitude = param2;

                location = new Location("chosen_location");
                location.setLatitude(latitude);
                location.setLongitude(longitude);
            }
        }

        MapFragment = new SupportMapFragment() {
            @Override
            public void onActivityCreated(Bundle savedInstanceState) {
                super.onActivityCreated(savedInstanceState);
                map = this.getMap();

                if(map != null) {
                    UiSettings mapUi = map.getUiSettings();
                    map.setOnMapLongClickListener(CreateEventMapFragment.this);
                    map.setMyLocationEnabled(true);
                    mapUi.setMyLocationButtonEnabled(true);
                    mapUi.setZoomControlsEnabled(true);

                    if(location != null) {
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        marker = map.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title(LatLngToString(latLng))
                                .draggable(true));
                        marker.showInfoWindow();
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                    }
                    else if(MainActivity.lastKnownLocation != null) {
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(MainActivity.lastKnownLocation.getLatitude(),
                                        MainActivity.lastKnownLocation.getLongitude()), 10));
                    }
                }
            }
        };

        getChildFragmentManager().beginTransaction().add(R.id.create_event_map, MapFragment).commit();

        autoCompView = (AutoCompleteTextView)view.findViewById(R.id.create_event_search);
        autoCompView.setAdapter(new PlacesAutoCompleteAdapter(this.getActivity(), R.layout.list_item));
        autoCompView.setMaxLines(1);
        autoCompView.setOnItemClickListener(this);

        button = (Button) view.findViewById(R.id.create_event_map_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickButton();
            }
        });

        Toast.makeText(this.getActivity().getApplicationContext(),
                "Press and hold down on the map to choose a location.", Toast.LENGTH_LONG).show();
        return view;
    }

    private void OnClickButton() {
        if(location == null) {
            Toast.makeText(this.getActivity().getApplicationContext(),
                    "Press and hold down on the map to choose a location.", Toast.LENGTH_LONG).show();
            return;
        }
        mListener.onMapInteraction(location);
        this.getActivity().getSupportFragmentManager().popBackStack();
    }

    private ArrayList<String> autocomplete(String input) {
        resultList = null;
        places = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:us");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            sb.append("&location=" + MainActivity.lastKnownLocation.getLatitude() +"," +
                    MainActivity.lastKnownLocation.getLongitude());
            sb.append("&radius=" + 20);

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            places = new ArrayList<String>(predsJsonArray.length());
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                places.add(predsJsonArray.getJSONObject(i).getString("place_id"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if(marker != null) {
            marker.remove();
        }


        marker = map.addMarker(new MarkerOptions()
                .position(latLng)
                //.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), x)))
                .title(LatLngToString(latLng))
                .draggable(true));
        marker.showInfoWindow();
        location = new Location("chosen_location");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    private String LatLngToString(LatLng latLng) {
        NumberFormat f = new DecimalFormat("#0.0000");
        String lat = f.format(latLng.latitude);
        String lon = f.format(latLng.longitude);
        return "lat/lng: (" + lat + "," + lon + ")";
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setMapListener(OnMapInteractionListener m ) {
        this.mListener = m;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        new GetLocation().execute(places.get(position));
        autoCompView.setText("");
    }


    public interface OnMapInteractionListener {
        // TODO: Update argument type and name
        public void onMapInteraction(Location location);
    }

    private class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    }
                    else {
                        notifyDataSetInvalidated();
                    }
                }};
            return filter;
        }
    }

    private class GetLocation extends AsyncTask<String, Void, LatLng> {
        @Override
        protected LatLng doInBackground(String... place_id) {

            LatLng toRet = null;
            HttpURLConnection conn = null;
            StringBuilder jsonResults = new StringBuilder();
            try {
                StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_DETAILS + OUT_JSON);
                sb.append("?input=bar");
                sb.append("&key=" + API_KEY);
                sb.append("&placeid=" + place_id[0]);
                URL url = new URL(sb.toString());
                conn = (HttpURLConnection) url.openConnection();
                System.out.println(sb.toString());

                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                // Load the results into a StringBuilder
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
            }
            catch(MalformedURLException e) {
                Log.e(LOG_TAG, "Error processing Places API URL", e);
                return null;
            }
            catch(IOException e) {
                Log.e(LOG_TAG, "Error connecting to Places API", e);
                return null;
            }
            finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }
            try {
                // Create a JSON object hierarchy from the results
                JSONObject jsonObj = new JSONObject(jsonResults.toString());
                JSONObject result = jsonObj.getJSONObject("result");
                JSONObject geometry = result.getJSONObject("geometry");
                JSONObject location = geometry.getJSONObject("location");
                toRet = new LatLng(location.getDouble("lat"),location.getDouble("lng"));
            } catch (JSONException e) {
                Log.e(LOG_TAG, "Cannot process JSON results", e);
            }
            return toRet;
        }

        @Override
        protected void onPostExecute(LatLng latLng) {
            if(latLng == null) {
                return;
            }

            if(marker != null) {
                marker.remove();
            }

            marker = map.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(LatLngToString(latLng))
                    .draggable(true));
            marker.showInfoWindow();
            location = new Location("chosen_location");
            location.setLatitude(latLng.latitude);
            location.setLongitude(latLng.longitude);

            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
        }
    }

}
