package com.floc.floc.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.floc.floc.Activities.MainActivity;
import com.floc.floc.CustomProfilePictureView;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyMapFragment extends Fragment implements OnMapReadyCallback {

    GoogleMap map;
    SupportMapFragment MapFragment;

    CameraPosition cameraPosition;
    boolean firstTime = true;
    private LruCache<String, Bitmap> mMemoryCache;

    static MyMapFragment mMapFragment;

    public static MyMapFragment getInstance() {
        if (mMapFragment == null) {
            mMapFragment = new MyMapFragment();
        }
        return mMapFragment;
    }

    public MyMapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        getActivity().setTitle("Map");

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };

        MapFragment = new SupportMapFragment() {
            @Override
            public void onActivityCreated(Bundle savedInstanceState) {
                super.onActivityCreated(savedInstanceState);
                map = this.getMap();

                if(map != null) {
                    UiSettings mapUi = map.getUiSettings();
                    //map.setMyLocationEnabled(true);
                   // mapUi.setMyLocationButtonEnabled(true);
                    mapUi.setZoomControlsEnabled(true);

                    if(firstTime && MainActivity.lastKnownLocation != null) {

                        ParseGeoPoint p = new ParseGeoPoint();
                        p.setLatitude(MainActivity.lastKnownLocation.getLatitude());
                        p.setLongitude(MainActivity.lastKnownLocation.getLongitude());

                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(MainActivity.lastKnownLocation.getLatitude(),
                                        MainActivity.lastKnownLocation.getLongitude()), 10));

                        firstTime = false;
                    }
                    FriendUpdater.run();
                }
            }
        };

        getChildFragmentManager().beginTransaction().add(R.id.create_event_map_fragment,
                MapFragment).commit();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraPosition = map.getCameraPosition();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (cameraPosition != null) {
            map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    public void addBitmapToMemory(String key, Bitmap bitmap) {
        if(getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this.getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("Location settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private final static int EVENTINTERVAL = 1000 * 5;
    Handler mFriendHandler = new Handler();

    private Runnable FriendUpdater = new Runnable() {
        @Override
        public void run() {
            updateFriends();
            mFriendHandler.postDelayed(FriendUpdater, EVENTINTERVAL);
        }
    };

    private void updateFriends() {
        ParseQuery query = ParseQuery.getQuery("_User");
        ArrayList<String> find = new ArrayList<>();
        find.add(ParseUser.getCurrentUser().getObjectId());
        query.whereContainedIn("friends", find );
        query.findInBackground(new FindCallback() {
            @Override
            public void done(List list, ParseException e) {
                if (e == null) {
                    map.clear();

                    if(MainActivity.lastKnownLocation != null) {
                        createMarkerBitmap(ParseApplication.userFacebookID, MainActivity.lastKnownLocation);
                    }

                    for (int i = 0; i < list.size(); i++) {
                        ParseUser user = (ParseUser)list.get(i);
                        if(user.getBoolean("locationEnabled")) {
                            ParseGeoPoint point = user.getParseGeoPoint("location");
                            String facebookID = user.getString("facebookID");
                            createMarkerBitmap(facebookID, point);
                        }
                    }
                }
                else {
                    Log.d(ParseApplication.TAG, e.toString());
                }
            }
        });
    }

    private void createMarkerBitmap(String facebookID, ParseGeoPoint point) {
        final Bitmap bitmap = getBitmapFromMemCache(facebookID);

        if(bitmap != null) {
            map.addMarker(new MarkerOptions()
                            .position(new LatLng(point.getLatitude(),
                                    point.getLongitude()))
                            .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
            );
        }
        else {
            PicInfo info = new PicInfo();
            info.facebookID = facebookID;
            info.latitude = point.getLatitude();
            info.longitude = point.getLongitude();
            new PicDownloader().execute(info);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    private class PicInfo {
        String facebookID;
        Double latitude;
        Double longitude;
    }
    public class PicDownloader extends AsyncTask<PicInfo, Void, Bitmap> {

        PicInfo info;

        @Override
        protected Bitmap doInBackground(PicInfo... params) {
            this.info = params[0];
            try {
                URL imageURL = new URL("https://graph.facebook.com/" + info.facebookID +
                        "/picture?type=large");
                Bitmap bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());

                View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                        inflate(R.layout.marker, null);
                ImageView image = (ImageView)view.findViewById(R.id.map_picture);
                image.setImageBitmap(CustomProfilePictureView.getRoundedBitmap(bitmap, false));
                Bitmap toReturn = createDrawableFromView(getActivity(), view);
                return toReturn;
            } catch (MalformedURLException e) {
                Log.d(ParseApplication.TAG, e.toString());
            } catch (IOException e) {
                Log.d(ParseApplication.TAG, e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if(bitmap != null) {
                addBitmapToMemory(info.facebookID, bitmap);

                map.addMarker(new MarkerOptions()
                        .position(new LatLng(info.latitude, info.longitude))
                        .icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
            }
        }
    }


    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }
}
