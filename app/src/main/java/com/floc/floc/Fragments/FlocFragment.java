package com.floc.floc.Fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.floc.floc.Floc;
import com.floc.floc.R;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FlocFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String userID;

    EditText search;
    ListView listView;
    Button button;
    ParseQueryAdapter<Floc> adapter;

    public static FlocFragment newInstance(String userID) {
        FlocFragment fragment = new FlocFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, userID);
        fragment.setArguments(args);
        return fragment;
    }

    public FlocFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userID = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_floc, container, false);
        // Inflate the layout for this fragment
        getActivity().setTitle("Flocs");


        button = (Button) view.findViewById(R.id.floc_button);
        listView = (ListView) view.findViewById(R.id.floc_listView);
        search = (EditText) view.findViewById(R.id.floc_searchbar);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickButton();
            }
        });

        adapter = new ParseQueryAdapter(getActivity(),  new ParseQueryAdapter.QueryFactory<Floc>() {
            public ParseQuery<Floc> create() {
                ParseQuery query = new ParseQuery("Groups");
                ArrayList<String> a = new ArrayList<>();
                a.add(ParseUser.getCurrentUser().getObjectId());
                query.whereContainedIn("Members",a);
                query.whereNotEqualTo("Name", "Select a floc to invite");
                query.orderByAscending("Name");
                return query;
            }
        }, android.R.layout.simple_list_item_2);

        adapter.setTextKey("Name");
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Floc floc = adapter.getItem(position);
                FlocMemberFragment fragment = FlocMemberFragment.newInstance(floc.getObjectId(), floc.getName());
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        });
        return view;
    }

    private void OnClickButton() {
        final EditText input = new EditText(getActivity());
        input.setHint("Name of floc");
        input.setBackgroundColor(0xd3d3d3);
        input.setSingleLine();
        new AlertDialog.Builder(getActivity())
                .setTitle("Create a floc")
                .setView(input)
                .setNegativeButton("Create", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().toString().equals("")) {
                            return;
                        }
                        final Floc floc = new Floc();
                        floc.setName(input.getText().toString());
                        floc.setDefault(false);
                        floc.addUnique("Members", ParseUser.getCurrentUser().getObjectId());

                        floc.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                ParseUser.getCurrentUser().addUnique("group", floc.getObjectId());
                                ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        adapter.loadObjects();
                                        adapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        });
                    }
                })
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }

                ).

                show();
    }


}
