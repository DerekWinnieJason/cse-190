package com.floc.floc.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.floc.floc.ListViewAdapters.FlocAdapter;
import com.floc.floc.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FlocMemberFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    String flocID;
    String name;
    ListView listView;
    Button button;
    EditText searchBar;

    public static FlocMemberFragment newInstance(String flocID, String name) {
        FlocMemberFragment fragment = new FlocMemberFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, flocID);
        args.putString(ARG_PARAM2, name);
        fragment.setArguments(args);
        return fragment;
    }

    public FlocMemberFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            flocID = getArguments().getString(ARG_PARAM1);
            name = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_floc_member, container, false);

        getActivity().setTitle(name);

        listView = (ListView) v.findViewById(R.id.floc_friend_listview);
        button = (Button) v.findViewById(R.id.floc_add_button);
        FlocAdapter adapter = new FlocAdapter(flocID, getActivity());
        listView.setAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FlocAddFriendFragment fragment = FlocAddFriendFragment.newInstance(flocID);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(null);
                transaction.commitAllowingStateLoss();
            }
        });

        return v;
    }


}
