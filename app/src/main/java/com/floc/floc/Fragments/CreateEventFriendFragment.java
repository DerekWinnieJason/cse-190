package com.floc.floc.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.floc.floc.ListViewAdapters.CustomFriendsAdapter;
import com.floc.floc.AdapterItems.Friend;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashSet;

public class CreateEventFriendFragment extends Fragment implements AbsListView.OnItemClickListener {

    private OnFriendInteractionListener mListener;

    private static final String ARG_PARAM1 = "param1";
    private CustomFriendsAdapter mAdapter;
    ProgressDialog dialog;
    private HashSet<String> invited_friends;
    ArrayList<Friend> friends;
    ListView listView;
    EditText SearchBar;

    public static CreateEventFriendFragment newInstance() {
        CreateEventFriendFragment fragment = new CreateEventFriendFragment();
        return fragment;
    }

    public void SetHashSet(HashSet<String> here) {
        invited_friends = here;
    }

    public CreateEventFriendFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String[] toAdd = getArguments().getStringArray(ARG_PARAM1);
            for(int i=0; i<toAdd.length; i++) {
                invited_friends.add(toAdd[i]);
            }
        }
        friends = new ArrayList<Friend>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_event_friend, container, false);

        Button invite_friends = (Button)view.findViewById(R.id.create_event_invite_friends);
        listView = (ListView)view.findViewById(R.id.create_event_friends_listview);
        SearchBar = (EditText)view.findViewById(R.id.create_event_friends_search);

        invite_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickInviteFriends();
            }
        });

        listView.setTextFilterEnabled(true);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        listView.setOnItemClickListener(this);

        SearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mAdapter != null) {
                    mAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    private void OnClickInviteFriends() {
        mListener.onFriendInteraction(invited_friends);
        this.getActivity().getSupportFragmentManager().popBackStack();
    }

    private void PopulateListView(final JSONArray array, final int num) {
        if(array == null) {
            dialog.dismiss();
            return;
        }
        if(num == array.length()) {
            mAdapter = new CustomFriendsAdapter(getActivity(),
                    R.layout.listview_friend_item, friends);
            listView.setAdapter(mAdapter);
            dialog.dismiss();
            return;
        }
        try {
            ParseQuery query = new ParseQuery("_User");
            query.getInBackground(array.getString(num), new GetCallback() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    Friend friend = new Friend();
                    friend.setFacebookID(parseObject.getString(("facebookID")));
                    friend.setName(parseObject.getString("name"));
                    friend.setObjectID(parseObject.getObjectId());
                    friends.add(friend);
                    PopulateListView(array, num + 1);
                }
            });
        }
        catch (JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        friends = new ArrayList<Friend>();
        dialog = ProgressDialog.show(this.getActivity(), "", "Loading friends...");
        ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                ParseQuery getFriends = new ParseQuery("_User");
                getFriends.getInBackground(ParseUser.getCurrentUser().getObjectId(), new GetCallback() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        JSONArray array = parseObject.getJSONArray("friends");
                        PopulateListView(array, 0);
                    }
                });
            }
        });
    }

    public void setFriendListener(OnFriendInteractionListener f) {
        this.mListener = f;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HashSet<String> set = new HashSet<String>();

        if(invited_friends.contains(friends.get(position).getObjectID())) {
            view.setBackgroundResource(R.color.white);
            invited_friends.remove(friends.get(position).getObjectID());
        }
        else {
            view.setBackgroundResource(R.color.theme);
            invited_friends.add(friends.get(position).getObjectID());
        }

    }

    public interface OnFriendInteractionListener {
        public void onFriendInteraction(HashSet<String> invited_friends);
    }

}
