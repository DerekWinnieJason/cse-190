package com.floc.floc.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.facebook.widget.ProfilePictureView;
import com.floc.floc.FlocEvent;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;


public class EventInvitationFragment extends Fragment {

    private String eventID;
    private TextView name;
    private TextView details;
    ProfilePictureView p;

    FlocEvent event;

    public EventInvitationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
/*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_invitation, container, false);

        name = (TextView) view.findViewById(R.id.event_name);
        details = (TextView) view.findViewById(R.id.event_description);
        p = (ProfilePictureView)view.findViewById(R.id.invite_userProfilePicture);

        // Inflate the layout for this fragment

        ParseQuery findObjectID = ParseQuery.getQuery("Events");
        findObjectID.whereEqualTo("objectId", eventID);
        findObjectID.findInBackground(new FindCallback() {
            @Override
            public void done(List list, ParseException e) {
                if(e == null) {
                    if(list.size() != 0) {
                        event = (FlocEvent) list.get(0);
                        setEventInformation(event);
                    }
                }
                else {
                    Log.d(ParseApplication.TAG, e.toString());
                }
            }
        });

        name.setText(event.getName());
        details.setText(event.getDetails());
        p.setProfileId(event.getCreatorFacebookID());

        Button accept = (Button) view.findViewById(R.id.accept_invitation);
        Button decline = (Button) view.findViewById(R.id.decline_invitation);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickAccept();
            }
        });

        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickDecline();
            }
        });
        return view;
    }

    private void OnClickAccept() {
        ParseUser.getCurrentUser().addUnique("accepted_events", eventID);
        List<String> removeFromUser = new ArrayList<String>();
        removeFromUser.add(eventID);
        //ParseUser.getCurrentUser().removeAll("declined_events", removeFromUser);
        ParseUser.getCurrentUser().saveInBackground();

        event.addUnique("Accepted", ParseUser.getCurrentUser().getObjectId());
        List<String> removeFromEvent = new ArrayList<String>();
        removeFromEvent.add(ParseUser.getCurrentUser().getObjectId());
        event.removeAll("Invited", removeFromEvent);
        event.removeAll("Declined", removeFromEvent);

        event.saveInBackground( new SaveCallback() {
            @Override
            public void done(ParseException e) {
                HashMap<String, Object> f = new HashMap<String, Object>();
                f.put("objectid", event.getCreator());
                f.put("eventid", eventID);
                f.put("acceptID", ParseUser.getCurrentUser().getObjectId());
                f.put("name", ParseApplication.userName);
                ParseCloud.callFunctionInBackground("AcceptedInvitationPushes", f);

                HashMap<String, Object> s = new HashMap<String, Object>();
                s.put("objectid", ParseUser.getCurrentUser().getObjectId());
                s.put("name", ParseApplication.userName);
                s.put("creator", event.getCreator());
                ParseCloud.callFunctionInBackground("FavoritesAcceptPushes", s);
            }
        });




        ParseQuery findObjectID = ParseQuery.getQuery("Events");
        findObjectID.whereEqualTo("objectId", eventID);
        findObjectID.findInBackground(new FindCallback() {
            @Override
            public void done(List list, ParseException e) {
                if(e == null) {
                    if(list.size() != 0) {
                        final FlocEvent event = (FlocEvent) list.get(0);
                        event.addUnique("Accepted", ParseUser.getCurrentUser().getObjectId());

                        List<String> toRemove = new ArrayList<String>();
                        toRemove.add(ParseUser.getCurrentUser().getObjectId());
                        event.removeAll("Invited", toRemove);

                        event.saveInBackground( new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                HashMap<String, Object> f = new HashMap<String, Object>();
                                f.put("objectid", event.getCreator());
                                f.put("eventid", eventID);
                                f.put("name", "Winnieeee");
                                ParseCloud.callFunctionInBackground("AcceptedInvitationPushes", f);

                                HashMap<String, Object> s = new HashMap<String, Object>();
                                s.put("objectid", ParseUser.getCurrentUser().getObjectId());
                                s.put("name", "JASONNN");
                                s.put("creator", event.getCreator());
                                ParseCloud.callFunctionInBackground("FavoritesAcceptPushes", s);
                            }
                        });

                        ParseApplication.Accepted_Events.add(event);
                    }
                }
            }
        });

        this.getActivity().getSupportFragmentManager().popBackStack();
    }
*/
    private void OnClickDecline() {
        ParseUser.getCurrentUser().addUnique("declined_events", eventID);
        List<String> removeFromUser = new ArrayList<String>();
        removeFromUser.add(eventID);
        ParseUser.getCurrentUser().removeAll("accepted_events", removeFromUser);

        ParseUser.getCurrentUser().saveInBackground();

        List<String> toRemove = new ArrayList<String>();
        toRemove.add(ParseUser.getCurrentUser().getObjectId());
        event.removeAll("Invited", toRemove);
        event.removeAll("Accepted", toRemove);
        event.addUnique("Declined", ParseUser.getCurrentUser().getObjectId());
        event.saveInBackground();

        /*
        ParseQuery findObjectID = ParseQuery.getQuery("Events");
        findObjectID.whereEqualTo("objectId", eventID);
        findObjectID.findInBackground(new FindCallback() {
            @Override
            public void done(List list, ParseException e) {
                if(e == null) {
                    if(list.size() != 0) {
                        FlocEvent event = (FlocEvent) list.get(0);

                        List<String> toRemove = new ArrayList<String>();
                        toRemove.add(ParseUser.getCurrentUser().getObjectId());
                        event.removeAll("Invited", toRemove);

                        event.addUnique("Declined", ParseUser.getCurrentUser().getObjectId());
                        event.saveInBackground();
                    }
                }
                else {
                    Log.d(ParseApplication.TAG, e.toString());
                }
            }
        });*/
        this.getActivity().getSupportFragmentManager().popBackStack();

    }

    public void setEvent(FlocEvent event) {
        //p.setProfileId(event.getCreatorFacebookID());
        //name.setText(event.getName());
        //details.setText(event.getDetails());
        this.event = event;
    }

}
