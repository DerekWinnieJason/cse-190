package com.floc.floc.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.floc.floc.ListViewAdapters.CustomFriendsAdapter;
import com.floc.floc.AdapterItems.Friend;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class FriendsListFragment extends Fragment implements AbsListView.OnItemClickListener,
    AbsListView.OnItemLongClickListener {
    private static final String ARG_PARAM1 = "param1";

    private CustomFriendsAdapter mAdapter;

    private String userID;
    EditText SearchBar;
    ProgressDialog dialog;
    ArrayList<Friend> friends;
    ListView listView;

    MessagingFragment messagingFragment;

    public static FriendsListFragment newInstance(String userID) {
        FriendsListFragment fragment = new FriendsListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, userID);
        fragment.setArguments(args);
        return fragment;
    }

    public FriendsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userID = getArguments().getString(ARG_PARAM1);
        }
        friends = new ArrayList<Friend>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends_list, container, false);
        getActivity().setTitle("Friends");
        Button findFriends = (Button)view.findViewById(R.id.find_friends);
        listView = (ListView)view.findViewById(R.id.friends_listview);
        SearchBar = (EditText)view.findViewById(R.id.friends_searchbar);

        findFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickFindFriends();
            }
        });

        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        SearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mAdapter != null) {
                    mAdapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    private void PopulateListView(final JSONArray array, final int num) {
        if(array == null) {
            dialog.dismiss();
            return;
        }
        if(num == array.length()) {
            mAdapter = new CustomFriendsAdapter(getActivity(),
                    R.layout.listview_friend_item, friends);
            listView.setAdapter(mAdapter);
            dialog.dismiss();
            return;
        }
        try {
            ParseQuery query = new ParseQuery("_User");
            query.getInBackground(array.getString(num), new GetCallback() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    Friend friend = new Friend();
                    friend.setFacebookID(parseObject.getString(("facebookID")));
                    friend.setName(parseObject.getString("name"));
                    friend.setObjectID(parseObject.getObjectId());
                    friends.add(friend);
                    PopulateListView(array, num + 1);
                }
            });
        }
        catch (JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
            dialog.dismiss();
        }

    }


    private void OnClickFindFriends() {
        FindFriendsFragment fragment = FindFriendsFragment.newInstance();
        DisplayFragment(fragment);
    }

    private void DisplayFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ProfileFragment fragment = ProfileFragment.newInstance(friends.get(position).getObjectID());
        DisplayFragment(fragment);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            messagingFragment = MessagingFragment.newInstance(friends.get(position).getObjectID(),
                    friends.get(position).getName());

        DisplayFragment(messagingFragment);
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        friends = new ArrayList<Friend>();
        dialog = ProgressDialog.show(this.getActivity(), "", "Loading friends...");
        ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                ParseQuery getFriends = new ParseQuery("_User");
                getFriends.getInBackground(userID, new GetCallback() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        if(e == null) {
                            JSONArray array = parseObject.getJSONArray("friends");
                            if (array != null) {
                                PopulateListView(array, 0);
                            }
                            else {
                                dialog.dismiss();
                            }
                        }
                        else {
                            dialog.dismiss();
                        }
                    }
                });
            }
        });
    }
}
