package com.floc.floc.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.floc.floc.ListViewAdapters.FlocAdapter;
import com.floc.floc.R;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class FlocAddFriendFragment extends Fragment {

    String flocID;
    private static final String ARG_PARAM1 = "param1";

    Boolean clicked = false;
    ArrayList<String> invites;
    Button button;
    ListView listView;

    public static FlocAddFriendFragment newInstance(String flocID) {
        FlocAddFriendFragment fragment = new FlocAddFriendFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, flocID);
        fragment.setArguments(args);
        return fragment;
    }


    public FlocAddFriendFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            flocID = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_event_friend, container, false);

        invites = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.create_event_friends_listview);
        button = (Button) view.findViewById(R.id.create_event_invite_friends);

        final FlocAdapter adapter = new FlocAdapter(flocID, getActivity(), "");
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(invites.contains(adapter.getItem(position).getObjectId())) {
                    invites.remove(adapter.getItem(position).getObjectId());
                    view.setBackgroundResource(R.color.white);
                }
                else {
                    invites.add(adapter.getItem(position).getObjectId());
                    view.setBackgroundResource(R.color.theme);
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickButton();
            }
        });
        return view;
    }

    private void OnClickButton() {
        if(clicked) return;
        clicked = true;
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("floc", flocID);
        params.put("friends", invites);
        ParseCloud.callFunctionInBackground("AddToFloc", params, new FunctionCallback<Object>() {
            @Override
            public void done(Object o, ParseException e) {
                ParseQuery query = new ParseQuery("Groups");
                query.getInBackground(flocID, new GetCallback() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        parseObject.addAllUnique("Members", invites);
                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                FlocAddFriendFragment.this.getActivity().getSupportFragmentManager().popBackStack();
                            }
                        });
                    }
                });

            }
        });
    }
}
