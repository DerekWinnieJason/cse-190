package com.floc.floc;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.floc.floc.Activities.MainActivity;
import com.parse.ParseCloud;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;

import java.util.HashMap;

/**
 * Created by Jason Wong on 3/4/2015.
 */
public class CustomLocationListener implements LocationListener {
    int num = 0;

    @Override
    public void onLocationChanged(Location location) {
        ParseGeoPoint toAdd = new ParseGeoPoint();
        toAdd.setLatitude(location.getLatitude());
        toAdd.setLongitude(location.getLongitude());
        MainActivity.lastKnownLocation = toAdd;
        ParseUser.getCurrentUser().put("location", toAdd);
        ParseUser.getCurrentUser().saveInBackground();

        if (ParseApplication.eventQueue.curr() != null) {
            final HashMap<String, Object> params = new HashMap<String, Object>();
            params.put("userid", ParseUser.getCurrentUser().getObjectId());
            params.put("eventid", ParseApplication.eventQueue.curr().getObjectId());
            ParseCloud.callFunctionInBackground("CallToMapQuest", params);
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
