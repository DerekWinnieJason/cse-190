package com.floc.floc;

import android.location.Location;
import android.util.Log;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Jason on 1/24/15.
 */
@ParseClassName("Events")
public class FlocEvent extends ParseObject {

    public FlocEvent() {
    }

    public void setName(String name) {
        put("Name", name);
    }

    public void setDetails(String details) {
        put("Details", details);
    }

    public void setLocationIsCreator(boolean creator_is_location) {
        put("Location_Is_Creator", creator_is_location);
    }
    public void setLocation(double latitude, double longitude) {
        ParseGeoPoint p = new ParseGeoPoint();
        p.setLatitude(latitude);
        p.setLongitude(longitude);
        put("Location", p);
    }

    public void setRadius(double radius) {
        put("Radius", radius);
    }

    public void setCreator(ParseUser user) {
        put("Creator", user.getObjectId());
    }

    public void setCreatorName(String name) { put("Creator_Name", name); }

    public void setCreatorFacebookID(ParseUser user) {
        put("CreatorFacebookID", user.get("facebookID").toString());
    }

    public void setInvited(String[] friends) {
        put("Invited", friends);
    }

    public String getName() {
        return getString("Name");
    }

    public String getDetails() {
        return getString("Details");
    }

    public boolean getLocationIsCreator() {
        return getBoolean("Location_Is_Creator");
    }

    public Location getLocation() {
        ParseGeoPoint p = getParseGeoPoint("Location");
        Location toRet = new Location("toReturn");
        toRet.setLatitude(p.getLatitude());
        toRet.setLongitude(p.getLongitude());
        return toRet;
    }

    public double getRadius() {
        return getDouble("Radius");
    }

    public String getCreator() {
        return getString("Creator");
    }

    public String getCreatorName() { return getString("Creator_Name"); }
    public String getCreatorFacebookID() { return getString("CreatorFacebookID"); }

    public ArrayList<String> getInvited() {
        ArrayList<String> toRet = new ArrayList<String>();
        try {
            JSONArray json = getJSONArray("Invited");

            if(json == null) return toRet;

            for (int i = 0; i < json.length(); i++) {
                toRet.add(json.getString(i));
            }
        }
        catch(JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
        return toRet;
    }

    public ArrayList<String> getAccepted() {
        ArrayList<String> toRet = new ArrayList<String>();
        try {
            JSONArray json = getJSONArray("Accepted");

            if(json == null) return toRet;

            for (int i = 0; i < json.length(); i++) {
                toRet.add(json.getString(i));
            }
        }
        catch(JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
        return toRet;
    }

    public ArrayList<String> getDeclined(){
        ArrayList<String> toRet = new ArrayList<String>();
        try {
            JSONArray json = getJSONArray("Declined");

            if(json == null) return toRet;

            for (int i = 0; i < json.length(); i++) {
                toRet.add(json.getString(i));
            }
        }
        catch(JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
        return toRet;
    }




}
