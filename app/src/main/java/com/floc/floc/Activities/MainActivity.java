package com.floc.floc.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.floc.floc.AdapterItems.NavItemInfo;
import com.floc.floc.CustomLocationListener;
import com.floc.floc.FlocEvent;
import com.floc.floc.Fragments.CreateEventFragment;
import com.floc.floc.Fragments.EventInfoFragment;
import com.floc.floc.Fragments.EventListFragment;
import com.floc.floc.Fragments.FlocFragment;
import com.floc.floc.Fragments.FriendsListFragment;
import com.floc.floc.Fragments.MessagingFragment;
import com.floc.floc.Fragments.MyMapFragment;
import com.floc.floc.Fragments.ProfileFragment;
import com.floc.floc.Fragments.StrangerProfileFragment;
import com.floc.floc.ListViewAdapters.CustomNavBarAdapter;
import com.floc.floc.MessageService;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class MainActivity extends ActionBarActivity  {

    ProfileFragment profileFragment;
    MyMapFragment mapFragment;
    EventListFragment eventListFragment;
    FriendsListFragment friendsListFragment;
    MessagingFragment messagingFragment;
    FlocFragment flocFragment;
    ParseImageView navImage;
    TextView navText;
    TextView hostText;

    boolean isUpdating = false;

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

    CustomLocationListener locationListener;
    LocationManager locationManager;
    public static ParseGeoPoint lastKnownLocation;

    ArrayList<NavItemInfo> infos;
    private CustomNavBarAdapter mAdapter;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Session session = ParseFacebookUtils.getSession();

        //getWindow().getDecorView().get
        setTitle("Map");
        //setTitleColor(0xffffff);

        if (session != null && session.isOpened()) {
        }
        else {
            startLoginActivity();
        }
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.right_drawer);
        navImage = (ParseImageView) findViewById(R.id.nav_image);
        navText = (TextView) findViewById(R.id.nav_text);
        hostText = (TextView) findViewById(R.id.nav_host);

        setUpLocationListener();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.parse.push.intent.RECEIVE");
        registerReceiver(receiver, filter);

        Intent serviceIntent = new Intent(getApplicationContext(), MessageService.class);
        startService(serviceIntent);
        OnMapFragment();
        eventHandler.run();

        /* TOCHANGE */
        arrived = new HashSet<>();
    }

    private void OnProfileFragment() {
        if(profileFragment == null) {
            profileFragment = ProfileFragment.newInstance(ParseUser.getCurrentUser().getObjectId());
        }
        DisplayFragment(profileFragment);
    }

    private void OnMapFragment() {
        mapFragment = MyMapFragment.getInstance();
        DisplayFragment(mapFragment);
    }

    private void OnFriendsFragment() {
        if(friendsListFragment == null) {
            friendsListFragment = FriendsListFragment.newInstance(ParseUser.getCurrentUser().getObjectId());
        }
        DisplayFragment(friendsListFragment);
    }

    private void OnCreateEventFragment() {
        CreateEventFragment createEventFragment = new CreateEventFragment();
        DisplayFragment(createEventFragment);
    }

    private void OnFlocFragment() {
        if(flocFragment == null) {
            flocFragment = FlocFragment.newInstance(ParseUser.getCurrentUser().getObjectId());
        }
        DisplayFragment(flocFragment);
    }

    public void OnClickEventCover(View v) {
        isUpdating = true;
        dialog = ProgressDialog.show(MainActivity.this, "", "Fetching event...", true);
        updateNavigationBar(ParseApplication.eventQueue.next());
    }

    private void DisplayFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

    public void OnClickProfile(View v) {
        OnProfileFragment();
    }

    public void OnClickMap(View v) {
        OnMapFragment();
    }

    public void OnClickFriends (View v) {
        OnFriendsFragment();
    }

    public void OnClickCreateEvent(View v) {
        OnCreateEventFragment();
    }

    public void OnClickMessages(View v) { /*startListUserActivity();*/OnFlocFragment();}

    /*
    private void startListUserActivity(){
        Intent intent = new Intent(getApplicationContext(), ListUsersActivity.class);
        Intent serviceIntent = new Intent(getApplicationContext(), MessageService.class);
        startService(serviceIntent);
        startActivity(intent);
    }
    */

    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void updateNavigationBar(FlocEvent event) {
        if(event == null) {
            navImage.setImageDrawable(getResources().getDrawable(R.drawable.default_cover));
            navText.setText("No active events");
            hostText.setText("");
        }
        else {
            infos = new ArrayList<>();

            JSONArray accepted = event.getJSONArray("Accepted");
            JSONArray duration = event.getJSONArray("Arrival_Time");
            JSONArray distance = event.getJSONArray("Arrival_Distance");
            JSONArray facebookIDs = event.getJSONArray("facebookIDs");

            updateListItems(accepted, distance, duration, facebookIDs);

            ParseFile photo = event.getParseFile("Picture");

            if(photo != null) {
                navImage.setParseFile(photo);
                navImage.loadInBackground();
            }
            else {
                navImage.setImageDrawable(getResources().getDrawable(R.drawable.default_event));
            }

            navText.setText(event.getName());
            hostText.setText("Host: " + event.getCreatorName());
        }
        navImage.setColorFilter(Color.rgb(80, 80, 80), PorterDuff.Mode.DARKEN);
    }

    /*
    private void updateNavigationBar() {
        if(ParseApplication.EventQueue.isEmpty()) {
            navImage.setImageDrawable(getResources().getDrawable(R.drawable.default_cover));
            navText.setText("No active events");
            hostText.setText("");
        }
        else {
            FlocEvent event = ParseApplication.EventQueue.getFirst();
            infos = new ArrayList<>();

            System.out.println("EVENT: " + event + " SIZE: " +  ParseApplication.EventQueue.size());
            JSONArray accepted = event.getJSONArray("Accepted");
            JSONArray duration = event.getJSONArray("Arrival_Time");
            JSONArray distance = event.getJSONArray("Arrival_Distance");

            addAccepted(accepted, distance, duration, 0);

            ParseFile photo = event.getParseFile("Picture");

            if(photo != null) {
                navImage.setParseFile(photo);
                navImage.loadInBackground();
            }
            else {
                navImage.setImageDrawable(getResources().getDrawable(R.drawable.default_event));
            }

            navText.setText(event.getName());
            hostText.setText("Host: " + event.getCreatorName());
        }
        navImage.setColorFilter(Color.rgb(80, 80, 80), PorterDuff.Mode.DARKEN);
    }
*/
    HashSet<String> arrived;

    private void updateListItems(JSONArray accepted, JSONArray distance, JSONArray duration,
                                 JSONArray facebookIDs ) {

        if (accepted == null || distance == null || duration == null || facebookIDs == null) {
            return;
        }

        try {
            for (int i = 0; i < accepted.length(); i++) {
                NavItemInfo info = new NavItemInfo();
                String number = distance.getString(i);
                if(calculateDistance(number)) {
                    info.setDistance("Arrived");
                    info.setTime("Arrived");

                    String id = accepted.getString(i);
                    if(!id.equals(ParseUser.getCurrentUser().getObjectId()) && !arrived.contains(id)) {
                        arriveAlert(id);
                        arrived.add(id);
                    }
                }
                else {
                    info.setDistance(number);
                    info.setTime(duration.getString(i));
                }

                info.setFacebookID(facebookIDs.getString(i));
                Log.d(ParseApplication.TAG, info.getFacebookID() +" , " + info.getDistance() + " , " + info.getTime());

                infos.add(info);
            }
        } catch (JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
        mAdapter = new CustomNavBarAdapter(this, R.layout.nav_user_item, infos);
        mAdapter.notifyDataSetChanged();
        mDrawerList.setAdapter(mAdapter);
        isUpdating = false;
        if(dialog != null) {
            dialog.dismiss();
        }
    }

    private boolean calculateDistance(String time) {
        String nums = time.replaceAll("\\D+","");
        int num = Integer.valueOf(nums);
        if(time.contains("ft")) {
            return true;
        }
        else if(num < 20) {
            return true;
        }
        return false;
    }
    /*
    private void addAccepted(final JSONArray accepted, final JSONArray distance,
                             final JSONArray duration, final int num) {
        if(accepted == null || distance == null || duration == null || num == accepted.length()) {
            mAdapter = new CustomNavBarAdapter(this, R.layout.nav_user_item, infos);
            mDrawerList.setAdapter(mAdapter);
            return;
        }
        try {
            // chagne to current user
            if (accepted.getString(num).equals(ParseUser.getCurrentUser().getObjectId())) {
                addAccepted(accepted, distance, duration, num + 1);
            }
            else {
                ParseQuery q = new ParseQuery("_User");
                q.getInBackground(accepted.getString(num), new GetCallback() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        try {
                            NavItemInfo info = new NavItemInfo();
                            info.setDistance(distance.getString(num));
                            info.setTime(duration.getString(num));
                            info.setFacebookID(parseObject.getString("facebookID"));
                            infos.add(info);
                            addAccepted(accepted, distance, duration, num + 1);
                        } catch(JSONException ex) {
                            Log.d(ParseApplication.TAG, ex.toString());
                        }
                    }
                });
            }
        } catch (JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
    }*/

    private void setUpLocationListener() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.parse.push.intent.RECEIVE");
        registerReceiver(receiver, filter);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setCostAllowed(false);

        String provider = locationManager.getBestProvider(criteria, false);

        Location location = locationManager.getLastKnownLocation(provider);

        locationListener = new CustomLocationListener();

        if(location != null) {
            locationListener.onLocationChanged(location);
        }
        else {
            Toast.makeText(this.getApplicationContext(), "Please turn on the GPS in your settings.",
                    Toast.LENGTH_LONG).show();        }
        locationManager.requestLocationUpdates(provider, 500, 20, locationListener);
    }

    private void updateViewsWithProfileInfo() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser.get("facebookID") != null) {
            //userProfilePictureView.setProfileId(facebookId);
        } else {
            // Show the default, blank user profile picture
            //userProfilePictureView.setProfileId(null);
        }
        // Set additional UI elements
    }


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extra = intent.getExtras();
            String json = extra.getString("com.parse.Data");
            Log.d(ParseApplication.TAG, json);
            JSONObject obj;
            String type;

            try {
                obj = new JSONObject(json);
                type = obj.getString("type");

                switch(type) {
                    case "invitation":
                        OnReceiveEventInvite(obj.getString("eventID"), obj.getString("name"));
                        break;
                    case "reply":
                        break;
                    case "friend_request":
                        OnReceiveFriendRequest(obj.getString("objectId"), obj.getString("name"));
                        break;
                }
            }
            catch (JSONException e) {
                Log.d(ParseApplication.TAG, e.toString());
            }
        }
    };

    private void UpdateUserEventResponse(String field, String eventID) {
        ArrayList<String> toRemove = new ArrayList<>();
        toRemove.add(eventID);
        ParseUser.getCurrentUser().removeAll("invited_events", toRemove);
        ParseUser.getCurrentUser().add(field, eventID);
        ParseUser.getCurrentUser().saveInBackground();
    }

    private void UpdateEventOnResponse(final String field, String eventID) {
        ParseQuery query = ParseQuery.getQuery("Events");
        query.getInBackground(eventID, new GetCallback() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                String id = ParseUser.getCurrentUser().getObjectId();
                ArrayList<String> toRemove = new ArrayList<>();
                toRemove.add(id);
                parseObject.removeAll("Invited", toRemove);
                parseObject.addUnique(field, id);
                parseObject.saveInBackground();
            }
        });
    }

    public void OnReceiveEventInvite(final String eventID, String name) {
        ParseUser.getCurrentUser().addUnique("invited_events", eventID);
        ParseUser.getCurrentUser().saveInBackground();

        new AlertDialog.Builder(this)
                .setTitle("Hangout with " + name)
                .setPositiveButton("Decline", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UpdateUserEventResponse("declined_events", eventID);
                        UpdateEventOnResponse("Declined", eventID);
                    }
                })
                .setNegativeButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UpdateUserEventResponse("accepted_events", eventID);
                        UpdateEventOnResponse("Accepted", eventID);
                        //EventInfoFragment fragment = EventInfoFragment.newInstance(eventID);
                        //DisplayFragment(fragment);
                    }
                })
                .setNeutralButton("Details", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EventInfoFragment fragment = EventInfoFragment.newInstance(eventID);
                        DisplayFragment(fragment);
                    }
                }).show();
    }

    private void OnReceiveFriendRequest(final String userID, String name) {

        new AlertDialog.Builder(this)
                .setTitle(name + " sent you a friend request!")
                .setPositiveButton("Decline", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ArrayList<String> toRemove = new ArrayList<>();
                        toRemove.add(userID);
                        ParseUser.getCurrentUser().removeAll("received_friend_request", toRemove);
                        ParseUser.getCurrentUser().saveInBackground();

                        HashMap<String, Object> params = new HashMap<>();
                        params.put("toSendRejection", userID);
                        params.put("rejected_from", ParseUser.getCurrentUser().getObjectId());

                        ParseCloud.callFunctionInBackground("RejectedFriendRequest", params);
                    }
                })
                .setNegativeButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HashMap<String, Object> params = new HashMap<>();
                        params.put("name", ParseApplication.userName);
                        params.put("request_from", userID);
                        params.put("request_to", ParseUser.getCurrentUser().getObjectId());

                        ParseCloud.callFunctionInBackground("AddFriends", params,
                                new FunctionCallback<Object>() {
                                    @Override
                                    public void done(Object o, ParseException e) {
                                        ParseUser.getCurrentUser().fetchInBackground();
                                    }
                                });
                        ProfileFragment fragment = ProfileFragment.newInstance(userID);

                        DisplayFragment(fragment);
                    }
                })
                .setNeutralButton("Profile", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        StrangerProfileFragment fragment = StrangerProfileFragment.newInstance(userID);
                        DisplayFragment(fragment);
                    }
                }).show();
    }


/*
    private void createNotification() {
        System.out.println("creating notification");
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!");

        int mNotificationId = 001;
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());

    }*/


    @Override
    public void onResume() {
        super.onResume();

        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
/*
            Bundle getData = getIntent().getExtras();
            String type = "menu_map";
            if(getData != null) {
                type = getData.getString("type");
                Log.d(ParseApplication.TAG, "TYPEIS: " + type);

            }
            else {

            }

            if(type.equals("event_invite")) {
                OnReceiveEventInvite(getData.getString("eventID"));
            }
*/
            IntentFilter filter = new IntentFilter();
            filter.addAction("com.parse.push.intent.RECEIVE");
            registerReceiver(receiver, filter);
        } else {
            // If the user is not logged in, go to the
            // activity showing the login view.
            startLoginActivity();
        }
    }

    @Override
    public void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            ParseUser.logOut();
            com.facebook.Session.getActiveSession().closeAndClearTokenInformation();
            startLoginActivity();
            mHandler.removeCallbacks(eventHandler);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    Handler mHandler = new Handler();

    private final static int LOCATIONINTERVAL = 1000 * 5;

    //private final static int QUEUEINTERVAL = 1000 * 10;

    private Runnable eventHandler = new Runnable() {

        private static final int updateNavigationBar = 1;
        private static final int updateEventQueue = 2;
        private int position = 1;

        @Override
        public void run() {
            if(!isUpdating && position == updateNavigationBar) {
                position = 2;
                updateNavigationBar(ParseApplication.eventQueue.curr());
            }
            else if(position == updateEventQueue) {
                position = 1;
                ParseApplication.updateEventQueue();
            }
            mHandler.postDelayed(eventHandler, LOCATIONINTERVAL);
        }
    };

    private void arriveAlert(String objectID) {
        ParseQuery q = ParseQuery.getQuery("_User");
        q.getInBackground(objectID, new GetCallback() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                String name = parseObject.getString("name");
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(name + " has arrived!")
                        .setPositiveButton("Message", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setNegativeButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });

    }

}
