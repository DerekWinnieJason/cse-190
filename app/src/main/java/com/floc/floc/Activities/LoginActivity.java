package com.floc.floc.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.floc.floc.ParseApplication;
import com.floc.floc.R;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import java.util.Arrays;
import java.util.List;

public class LoginActivity extends ActionBarActivity {

    private Dialog loginDialog;
    private Dialog infoDialog;

    private int numCompleted = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Check if there is a currently logged in user
        // and it's linked to a Facebook account.
        ParseUser currentUser = ParseUser.getCurrentUser();
        if ((currentUser != null) && ParseFacebookUtils.isLinked(currentUser)) {
            // Go to the user info activity
            showUserDetailsActivity();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);
    }

    public void onLoginClick(View v) {
        loginDialog = ProgressDialog.show(LoginActivity.this, "", "Logging in...", true);

        List<String> permissions = Arrays.asList("public_profile", "email");
        // NOTE: for extended permissions, like "user_about_me", your app must be reviewed by the Facebook team
        // (https://developers.facebook.com/docs/facebook-login/permissions/)

        ParseFacebookUtils.logIn(permissions, this, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (user == null) {
                    Log.d(ParseApplication.TAG, "Uh oh. The user cancelled the Facebook login.");
                } else {
                    Log.d(ParseApplication.TAG, "User logged in through Facebook!");
                    makeMeRequest();
                }
            }
        });
        loginDialog.dismiss();
    }

    private void makeMeRequest() {
        Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        if (user != null) {

                            ParseUser.getCurrentUser().put("name", user.getName());
                            ParseUser.getCurrentUser().put("facebookID", user.getId());
                            ParseUser.getCurrentUser().put("locationEnabled", true);
                            ParseUser.getCurrentUser().saveInBackground();

                            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                            installation.put("user",ParseUser.getCurrentUser());
                            installation.saveInBackground();

                            showUserDetailsActivity();
                        } else if (response.getError() != null) {
                        Log.d(ParseApplication.TAG, response.getError().toString());
                        }
                    }
                });
        request.executeAsync();
    }

    private void showUserDetailsActivity() {
        infoDialog = ProgressDialog.show(LoginActivity.this, "", "Downloading profile...", true);
        ParseApplication.userName = ParseUser.getCurrentUser().getString("name");
        ParseApplication.userFacebookID = ParseUser.getCurrentUser().getString("facebookID");
        ParseApplication.updateEventQueue();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        infoDialog.dismiss();

    }

}
