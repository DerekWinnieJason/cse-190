package com.floc.floc;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jason Wong on 2/6/2015.
 */
public class CustomParseBroadcastReceiver extends ParsePushBroadcastReceiver  {
    String type;
    String objectID;

    @Override
    protected void onPushOpen(Context context, Intent intent) {

        Bundle extra = intent.getExtras();
        String json = extra.getString("com.parse.Data");
        JSONObject obj;

        try {
            obj = new JSONObject(json);
            type = obj.getString("type");

            switch(type) {
                case "invitation":
                    intent.putExtra("type", "event_invite");
                    intent.putExtra("eventID", obj.getString("eventID"));
                    break;

                case "reply":
                    intent.putExtra("type", "reply");
                    intent.putExtra("eventID", obj.getString("eventID"));
                    break;

            }
        } catch (JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //intent.setAction(Intent.ACTION_MAIN);
        //intent.addCategory(Intent.CATEGORY_LAUNCHER);
        //intent.setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);//Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        //intent.
        super.onPushOpen(context, intent);
    }


    @Override
    protected void onPushReceive(Context context, Intent intent) {
/*

        if(!ParseApplication.isAppInForeground((context))) {
            super.onPushReceive(context, intent);
        }*/
       // super.onPushReceive(context, intent);

        Bundle extra = intent.getExtras();
        String json = extra.getString("com.parse.Data");
        JSONObject obj;

        Boolean showNotification = true;

        try {
            obj = new JSONObject(json);
            type = obj.getString("type");

            switch(type) {
                case "invitation":
                    if(ParseApplication.isAppInForeground(context)) {

                    }
                    break;

                case "reply":
                    break;
            }
        } catch (JSONException e) {
            Log.d(ParseApplication.TAG, e.toString());
        }

        if(showNotification) {
            super.onPushReceive(context, intent);
        }
    }
}
