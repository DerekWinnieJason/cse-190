package com.floc.floc;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Created by Jason Wong on 3/18/2015.
 */
@ParseClassName("Groups")
public class Floc extends ParseObject {

    public Floc() {

    }

    public void setName(String name) {
        put("Name", name);
    }

    public void setMembers(String[] members) {
        put("Members", members);
    }

    public void setDefault(boolean d) { put ("default", d); }

    public String getName() {
        return getString("Name");
    }
}
