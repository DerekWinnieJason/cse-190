package com.floc.floc.ListViewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.floc.floc.CustomProfilePictureView;
import com.floc.floc.AdapterItems.Friend;
import com.floc.floc.R;

import java.util.ArrayList;

/**
 * Created by Jason on 1/30/15.
 */
public class CustomFriendsAdapter extends ArrayAdapter {
    private ArrayList<Friend> originalFriends;
    private ArrayList<Friend> filteredFriends;
    private FriendFilter filter;

    private class ViewHolder {
        CustomProfilePictureView p;
        TextView name;
    }
    public CustomFriendsAdapter(Context context, int resource, ArrayList<Friend> f) {
        super(context, resource, f);
        this.originalFriends = new ArrayList<Friend>();
        this.filteredFriends = new ArrayList<Friend>();
        this.originalFriends.addAll(f);
        this.filteredFriends.addAll(f);
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent){
        ViewHolder holder = null;
        if(convertView==null) {
            LayoutInflater vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.listview_friend_item, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.listView_friendName);
            holder.p = (CustomProfilePictureView) convertView.findViewById(R.id.listView_userProfilePicture);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Friend friend = filteredFriends.get(pos);

        holder.name.setText(friend.getName());
        holder.p.setBorder(false);
        holder.p.setProfileId(friend.getFacebookID());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if(filter == null) {
            filter = new FriendFilter();
        }
        return filter;
    }

    private class FriendFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();

            if(constraint != null && constraint.toString().length() > 0) {
                ArrayList<Friend> f = new ArrayList<Friend>();
                for(int i=0, l = originalFriends.size(); i<l; i++) {
                    Friend friend = originalFriends.get(i);
                    if(friend.getName().toLowerCase().contains(constraint)) {
                        f.add(friend);
                    }
                }

                result.count = f.size();
                result.values = f;
            }
            else {
                synchronized (this) {
                    result.values = originalFriends;
                    result.count = originalFriends.size();
                }
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredFriends = (ArrayList<Friend>) results.values;
            notifyDataSetChanged();
            clear();
            for(int i=0, l=filteredFriends.size(); i<l; i++) {
                add(filteredFriends.get(i));
            }
            notifyDataSetInvalidated();
        }
    }

}
