package com.floc.floc.ListViewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.floc.floc.CustomProfilePictureView;
import com.floc.floc.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;

/**
 * Created by Jason on 1/30/15.
 */
public class HorizontalViewAdapter extends ArrayAdapter {
    ArrayList<String> ids;
    String type;

    private class ViewHolder {
        CustomProfilePictureView p;
    }

    public HorizontalViewAdapter(Context context, int resource, String type, ArrayList<String> ids) {
        super(context, resource, ids);
        this.type = type;
        this.ids = ids;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        ViewHolder holder = null;
        if(convertView==null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(type == "profile") {
                convertView = vi.inflate(R.layout.profilepicture, null);
                holder = new ViewHolder();
                holder.p = (CustomProfilePictureView) convertView.findViewById(R.id.profile_pic);

            }
            else if(type == "info") {
                convertView = vi.inflate(R.layout.profilepicture_event_info, null);
                holder = new ViewHolder();
                holder.p = (CustomProfilePictureView) convertView.findViewById(R.id.event_info_profilepicture);
            }

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ViewHolder h = holder;

        final String facebookID = ids.get(pos);

        ParseQuery query = ParseQuery.getQuery("_User");
        query.getInBackground(facebookID, new GetCallback() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if(e == null) {
                    h.p.setProfileId(parseObject.getString("facebookID"));
                }
                else {
                    System.out.println(e.toString());
                }
            }
        });

        return convertView;
    }

    public void delete(String objectID) {


    }

}
