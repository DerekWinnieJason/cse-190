package com.floc.floc.ListViewAdapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.floc.floc.CustomProfilePictureView;
import com.floc.floc.R;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import java.util.ArrayList;

/**
 * Created by Jason Wong on 3/18/2015.
 */
public class FlocAdapter extends ParseQueryAdapter<ParseUser> {

    public FlocAdapter(final String flocID, Context context) {
        super(context,  new ParseQueryAdapter.QueryFactory<ParseUser>() {
            public ParseQuery<ParseUser> create() {
                ParseQuery query = new ParseQuery("_User");
                ArrayList<String> a = new ArrayList<>();
                a.add(flocID);
                query.whereContainedIn("group",a);
                //query.orderByAscending("name");
                return query;
            }
        });
    }

    public FlocAdapter(final String flocID, Context context, String x) {
        super(context,  new ParseQueryAdapter.QueryFactory<ParseUser>() {
            public ParseQuery<ParseUser> create() {
                ParseQuery query = new ParseQuery("_User");
                ArrayList<String> a = new ArrayList<>();
                a.add(flocID);
                ArrayList<String> b = new ArrayList<>();
                b.add(ParseUser.getCurrentUser().getObjectId());
                query.whereNotContainedIn("group",a);
                query.whereContainedIn("friends", b);
                //query.orderByAscending("name");
                return query;
            }
        });
    }

    @Override
    public View getItemView(ParseUser user, View v, ViewGroup parent) {
        if (v == null) {
            v = View.inflate(getContext(), R.layout.listview_friend_item, null);
        }
        super.getItemView(user, v, parent);

        TextView textView = (TextView) v.findViewById(R.id.listView_friendName);
        CustomProfilePictureView p = (CustomProfilePictureView) v.findViewById(R.id.listView_userProfilePicture);
        p.setBorder(false);

        textView.setText(user.getString("name"));
        p.setProfileId(user.getString("facebookID"));

        return v;
    }
}
