package com.floc.floc.ListViewAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.floc.floc.CustomProfilePictureView;
import com.floc.floc.AdapterItems.NavItemInfo;
import com.floc.floc.R;

import java.util.ArrayList;

/**
 * Created by Jason Wong on 3/5/2015.
 */
public class CustomNavBarAdapter extends ArrayAdapter {

    private ArrayList<NavItemInfo> infos;

    private class ViewHolder {
        CustomProfilePictureView p;
        TextView distance;
        TextView time;
    }

    public CustomNavBarAdapter(Context context, int resource, ArrayList<NavItemInfo> infos) {
        super(context, resource, infos);
        this.infos = infos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.nav_user_item, null);
            holder = new ViewHolder();
            holder.distance = (TextView) convertView.findViewById(R.id.nav_item_distance);
            holder.time = (TextView) convertView.findViewById(R.id.nav_item_time);
            holder.p = (CustomProfilePictureView) convertView.findViewById(R.id.nav_item_pic);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        NavItemInfo info = infos.get(pos);

        holder.distance.setText(info.getDistance());
        holder.time.setText(info.getTime());
        holder.p.setProfileId(info.getFacebookID());
        convertView.setBackgroundColor(0xAA66CC);

        return convertView;
    }
}
