package com.floc.floc.ListViewAdapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.floc.floc.AdapterItems.EventItemInfo;
import com.floc.floc.R;
import com.meetme.android.horizontallistview.HorizontalListView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;

/**
 * Created by Jason on 1/30/15.
 */
public class CustomEventsAdapter extends ArrayAdapter {
    ArrayList<EventItemInfo> events;

    private class ViewHolder {
        CircularImageView image;
        TextView name;
        TextView creator;
        HorizontalListView listView;
    }

    public CustomEventsAdapter(Context context, int resource, ArrayList<EventItemInfo> events) {
        super(context, resource, events);
        this.events = events;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        ViewHolder holder = null;
        if(convertView==null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.profile_event_item, null);
            holder = new ViewHolder();
            holder.image = (CircularImageView) convertView.findViewById(R.id.profile_event_image);
            holder.name = (TextView) convertView.findViewById(R.id.profile_event_name);
            holder.creator = (TextView) convertView.findViewById(R.id.profile_event_creator);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        EventItemInfo event = events.get(pos);

        final ViewHolder h = holder;

        ParseQuery query = ParseQuery.getQuery("Events");
        query.getInBackground(event.getEventID(), new GetCallback() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                ParseFile pic = parseObject.getParseFile("Picture");

                if(pic == null) {
                    h.image.setImageDrawable(getContext().getResources().getDrawable(R.drawable.default_event));
                }
                else {
                    pic.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] bytes, ParseException e) {
                            if(e == null) {
                                Bitmap bmp;
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inMutable = true;
                                bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
                                h.image.setImageBitmap(bmp);
                            }
                        }
                    });
                }
            }
        });

        holder.name.setText(event.getEventName());
        holder.creator.setText(event.getEventCreator());

        return convertView;
    }

    public void delete(String objectID) {


    }

}
