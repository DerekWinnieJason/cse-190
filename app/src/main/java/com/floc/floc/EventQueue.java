package com.floc.floc;

import android.util.Log;

public class EventQueue {
    int pointer;
    int top;
    int bottom;
    int size;
    FlocEvent[] array;

    public EventQueue() {
        array = new FlocEvent[10];
        top = 0;
        bottom = 0;
        size = 0;
        pointer = 0;
    }

    public void enqueue(FlocEvent e) {
        if(contains(e)) return;
        if(size == array.length) resizeArray();
        array[top] = e;
        top = (top + 1) % array.length;
        size++;
       // Log.d(ParseApplication.TAG, "JUST ADDED: " + e.getName());
    }

    public FlocEvent dequeue() {
        if(size == 0) return null;
        FlocEvent toRet = array[bottom];
        bottom = (bottom + 1) % array.length;
        size--;
        return toRet;
    }

    public boolean contains(FlocEvent event) {
        for(int i=bottom; i != top; i = (i+1) % array.length) {
            if(array[i].getObjectId().equals(event.getObjectId())) {
                return true;
            }
        }
        return false;
    }

    public FlocEvent curr() {
        if(size == 0) return null;
        Log.d(ParseApplication.TAG, "POINTER IS : " + pointer);
        return array[pointer];
    }

    public FlocEvent next() {
        if(size == 0) return null;
        FlocEvent toRet = array[pointer];
        pointer = (pointer + 1) % size;
        Log.d(ParseApplication.TAG, "POINTER NEXT IS : " + pointer);

        return toRet;
    }

    public FlocEvent back() {
        if(size == 0) return null;
        pointer = (pointer - 1) % size;
        return array[pointer];
    }

    public void resizeArray() {
        FlocEvent[] bigger = new FlocEvent[array.length * 2];

        for(int i=0; i<array.length; i++) {
            bigger[i] = array[i];
        }
        top = top + array.length;
        array = bigger;
    }

    public FlocEvent[] toArray() {
        FlocEvent[] toRet = new FlocEvent[size];

        for(int i=bottom; i != top; i = (i+1) % array.length) {
            toRet[i] = array[i];
        }
        return toRet;
    }
}