package com.floc.floc;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jason on 1/20/15.
 */
public class ParseApplication extends Application {
    public static final String TAG = "floc";

    private static final String appID = "cz0OsFuJQEHyMNiRXGuRlgB9eOG88R2Ht5OAwOuu";
    private static final String clientID = "TFuGja60blCChjsgt3Dc8teRx1vPS51czIduHS6x";
    private static final String FacebookID = "374494942722874";

    public static String userName;
    public static String userFacebookID;

    public static EventQueue eventQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        ParseObject.registerSubclass(FlocEvent.class);
        ParseObject.registerSubclass(Floc.class);

        Parse.initialize(this, appID, clientID);
        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseFacebookUtils.initialize(getResources().getString(R.string.FacebookID));

        eventQueue = new EventQueue();
    }

    public static void updateEventQueue() {
        ArrayList<String> user = new ArrayList<>();
        user.add(ParseUser.getCurrentUser().getObjectId());
        int pointer = eventQueue.pointer;
        eventQueue = new EventQueue();
        eventQueue.pointer = pointer;
        /*
        ArrayList<String> events = new ArrayList<>();
        FlocEvent[] e = eventQueue.toArray();

        for(int i=0; i<e.length; i++) {
            events.add(e[i].getObjectId());
        }*/

        /** TODO THIS QUERY IS RETURNING DUPLCIATE **/
        ParseQuery query = ParseQuery.getQuery("Events");
        query.whereContainedIn("Accepted", user);
        //query.whereNotContainedIn("objectId", events);

        query.findInBackground(new FindCallback() {
            @Override
            public void done(List list, ParseException e) {
                for (int i = 0; i < list.size(); i++) {
                    eventQueue.enqueue((FlocEvent) list.get(i));
                }
            }
        });

    }

    public static boolean isAppInForeground(Context context) {

        try {
            String task = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).
                    getRunningTasks(1).get(0).topActivity.getPackageName();
            if(task.equals("com.floc.floc")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            return false;
        }
    }

}
