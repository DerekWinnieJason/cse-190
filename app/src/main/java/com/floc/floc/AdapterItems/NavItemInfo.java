package com.floc.floc.AdapterItems;

/**
 * Created by Jason Wong on 3/5/2015.
 */
public class NavItemInfo {
    private String facebookID;
    private String distance;
    private String time;

    public String getFacebookID() {
        return facebookID;
    }

    public void setFacebookID(String facebookID) {
        this.facebookID = facebookID;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
