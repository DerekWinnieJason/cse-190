package com.floc.floc.AdapterItems;

/**
 * Created by Jason Wong on 3/7/2015.
 */
public class EventItemInfo {
    private String eventID;
    private String eventName;
    private String eventCreator;

    public String getEventID() {
        return eventID;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventCreator() {
        return eventCreator;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setEventCreator(String eventCreator) {
        this.eventCreator = eventCreator;
    }
}
