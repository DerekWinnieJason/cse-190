package com.floc.floc.AdapterItems;

/**
 * Created by Jason Wong on 2/25/2015.
 */
public class Friend {
    private String name;
    private String objectID;
    private String facebookID;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    public String getFacebookID() {
        return facebookID;
    }

    public void setFacebookID(String facebookID) {
        this.facebookID = facebookID;
    }

    public String toString() {
        return this.name;
    }
}
